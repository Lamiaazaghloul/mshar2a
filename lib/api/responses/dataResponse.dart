class ApiDataResponse<TType> {
  int status;
  TType data;
  String error;
  String key;
  ApiDataResponse({
    this.status,
    this.data,
    this.key,
    this.error = "No internet or failed to get data",
  });

  factory ApiDataResponse.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;
    return ApiDataResponse(
      status: json['status'] is int
          ? json['status']
          : (json['status'] == true ? 200 : 400),
      data: json['data'],
      error: ((json['error'] as String) ?? (json['message'] as String)) ??
          (json['msg'] as String),
      key: json['key'] ?? "",
    );
  }

  @override
  String toString() {
    return error ?? "There is no error";
  }
}
