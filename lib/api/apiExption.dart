
import 'responses/dataResponse.dart';

class ApiFetchException implements Exception {
  final String message;
  final ApiDataResponse<dynamic> response;
  ApiFetchException(this.message, this.response);
  @override
  String toString() {
    return message??response?.error;
  }
}
