import 'dart:async';
import 'dart:io';
import 'package:demo/Tools/print.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:path/path.dart';

import 'apiExption.dart';
import 'authHelper.dart';
import 'responses/dataResponse.dart';
import 'responses/listResponse.dart';

//! help method

void onSendProgress(int received, int total, Function(double) onProgress) {
  if (total != -1 && onProgress != null) {
    onProgress((received / total * 100));
  }
}

void mapErrorResponse(dynamic e, {Map<String, dynamic> data}) {
  var response = ApiDataResponse.fromJson(data);
  if (e is DioError) {
    DioError error = e;
    if (error.type == DioErrorType.DEFAULT)
      throw ApiFetchException(ApiHelper.instance.noInternetMessage, response);
    else if (error.type == DioErrorType.CONNECT_TIMEOUT)
      throw ApiFetchException(
          ApiHelper.instance.connectionTimeOutMessage, response);
    else if (error.type == DioErrorType.RECEIVE_TIMEOUT)
      throw ApiFetchException(
          ApiHelper.instance.recivingTimeOutMessage, response);
    else if (error.type == DioErrorType.SEND_TIMEOUT)
      throw ApiFetchException(
          ApiHelper.instance.sendingTimeOutMessage, response);
    else if (error.type == DioErrorType.RESPONSE)
      throw ApiFetchException(ApiHelper.instance.notSuccessResponse, response);
    else if (error.type != DioErrorType.CANCEL)
      throw ApiFetchException(error.message, response);
  } else
    throw ApiFetchException(e.toString(), response);
}

//* Requests

class ApiHelper {
  @protected
  static ApiHelper _instance;
  @protected
  final String baseUrl;

  @protected
  final String noInternetMessage;
  @protected
  final String connectionTimeOutMessage;
  @protected
  final String sendingTimeOutMessage;
  @protected
  final String notSuccessResponse;
  @protected
  final String recivingTimeOutMessage;
  @protected
  final String authorizationMessage;

  @protected
  final String listTotalKey;
  @protected
  final bool requestHeadersAsParms;

  @protected
  final Map<String, dynamic> Function(int skip, int pageSize) paging;
  static ApiHelper get instance => _instance ?? ApiHelper.intialize();

  String get base {
    return this.baseUrl;
  }

  ApiHelper.intialize({
    // http://agaza.work/
    this.baseUrl = "http://google.com",
    this.paging,
    this.listTotalKey = "total",
    this.requestHeadersAsParms = false,
    this.noInternetMessage = "No internet or failed to get data",
    this.notSuccessResponse = "Failed to get data",
    this.authorizationMessage =
        "Sorry you are unauthorized  please contact us or try to clear your cash",
    this.sendingTimeOutMessage =
        "Sending data takes too long time please check your internet",
    this.connectionTimeOutMessage =
        "Connection takes too long time please check your internet",
    this.recivingTimeOutMessage =
        "Request made but the server take to long time to response",
  }) {
    _instance = this;
  }
  @protected
  Future<Options> _getRequestOptions(
      [int skip, Map<String, dynamic> params]) async {
    // var headers = await AuthHelper.instance.getAuthHeaders();
    // if (this.requestHeadersAsParms == true && params != null) {
    //   params.addAll(headers);
    // }
    Map<String,dynamic> headers={};
    headers["Accept"] = "application/json";
    if (skip != null) headers["skip"] = skip.toString();
    var options = Options(
      headers: headers,
      connectTimeout: 30000,
      receiveTimeout: 100000,
      sendTimeout: 100000,
      validateStatus: (s) => true,
      receiveDataWhenStatusError: true,
    );
    return options;
  }

  @protected
  Dio _getDioRequest() {
    Dio dio = new Dio()..options.baseUrl = baseUrl;
    return dio;
  }

  Future<ApiListResponse> fetchListItems(String url,
      {int skip = 0, int pageSize = 20, Map<String, dynamic> params}) async {
    var _parms = params ?? Map<String, dynamic>();
    _parms["pageSize"] = pageSize;
    _parms["skip"] = skip;
    _parms["skipCount"] = skip;
    _parms.removeWhere((key, v) => v == null);
    printYellow(url);
    printYellow(_parms);

    var response = await _getDioRequest()
        .get<Map<String, dynamic>>(url,
            queryParameters: _parms,
            options: await _getRequestOptions(skip, _parms))
        .catchError(mapErrorResponse);
    ApiListResponse res = ApiListResponse.fromJson(response.data,skip, listTotalKey);
    if (response.statusCode == 401)
      throw ApiFetchException(
          this.authorizationMessage, ApiDataResponse.fromJson({"status": 401}));
    res.status = res.status ?? (res.data != null ? 200 : 400);
    if (response.statusCode == 200 && res.status == 200) {
      printGreen(response.data);
      return res;
    } else {
      printRed(response.data);
      throw ApiFetchException(
          res.error, ApiDataResponse.fromJson(response.data));
    }
  }

  Future<ApiDataResponse<Map<String, dynamic>>> fetchSingleObject(String url,
      {Map<String, dynamic> params}) async {
    var _params = params ?? {};
    var response = await _getDioRequest()
        .get<Map<String, dynamic>>(url,
            queryParameters: _params,
            options: await _getRequestOptions(null, _params))
        .catchError(mapErrorResponse);
    ApiDataResponse<Map<String, dynamic>> res =
        ApiDataResponse<Map<String, dynamic>>.fromJson(response.data);
    if (response.statusCode == 401)
      throw ApiFetchException(
          this.authorizationMessage, ApiDataResponse.fromJson({"status": 401}));
    res.status = res.status ?? (res.data != null ? 200 : 400);
    if (response.statusCode == 200 && res.status == 200) {
      printGreen(response.data);
      return res;
    } else {
      printRed(response.data);
      throw ApiFetchException(res.error, res);
    }
  }

  Future<ApiDataResponse<dynamic>> fetchAny(String url,
      {Map<String, String> params,
      Map<String, dynamic> Function(Response<dynamic> response)
          mapResponse}) async {
    var _params = params ?? {};
    var response = await _getDioRequest()
        .get<dynamic>(url,
            queryParameters: _params,
            options: await _getRequestOptions(null, _params))
        .catchError(mapErrorResponse);
    var data = mapResponse == null
        ? (response.data is Map ? response.data : null)
        : mapResponse(response);
    ApiDataResponse<dynamic> res = ApiDataResponse<dynamic>.fromJson(data);
    if (response.statusCode == 401)
      throw ApiFetchException(
          this.authorizationMessage, ApiDataResponse.fromJson({"status": 401}));

    res.status = res.status ?? (res.data != null ? 200 : 400);
    if (response.statusCode == 200 && res.status == 200) {
      printGreen(response.data);
      return res;
    } else {
      printRed(response.data);
      throw ApiFetchException(res.error, res);
    }
  }

  Future<ApiDataResponse<TType>> postJsonData<TType>(
      String url, Map<String, dynamic> data,
      {Map<String, String> params,
      Map<String, dynamic> Function(Response<dynamic> response)
          mapResponse}) async {
    var _params = params ?? {};
    var response = await _getDioRequest()
        .post<Map<String, dynamic>>(url,
            data: data,
            queryParameters: _params,
            options: await _getRequestOptions(null, _params))
        .catchError(mapErrorResponse);
    var resData = mapResponse == null ? response.data : mapResponse(response);
    ApiDataResponse<TType> res = ApiDataResponse<TType>.fromJson(resData);
    if (response.statusCode == 401)
      throw ApiFetchException(
          this.authorizationMessage, ApiDataResponse.fromJson({"status": 401}));
    res.status = res.status ?? (res.data != null ? 200 : 400);
    if (response.statusCode == 200 && res.status == 200) {
      printGreen(response.data);
      if (res.data is Map<String, dynamic>) {
        await AuthHelper.instance
            .authenticateDevice(res.data as Map<String, dynamic>);
      }
      return res;
    } else {
      printRed(response.data);
      throw ApiFetchException(res.error, res);
    }
  }

  final Map<String, CancelToken> cancelTokens = {};

  Future<ApiDataResponse<TType>> postWithMultiFiles<TType>(
      String url, List<File> files,
      {Map<String, dynamic> data,
      String filesKey,
      Function(double) onProgress,
      String cancelToken,
      Map<String, String> params}) async {
    FormData formData = new FormData.from(data ?? {});
    if (files != null && files.length > 0)
      formData.add(
          filesKey ?? "files[]",
          files.where((w) => w != null).map((file) {
            return UploadFileInfo(file, basename(file.path));
          }).toList());
    CancelToken cntoken;
    if (cancelToken != null) {
      if (cancelTokens[cancelToken] != null) cancelTokens[cancelToken].cancel();
      cancelTokens[cancelToken] = CancelToken();
      cntoken = cancelTokens[cancelToken];
    }
    var _params = params ?? {};
    printYellow(baseUrl+url);
    var response = await _getDioRequest()
        .post<String>(url,
            data: formData,
            cancelToken: cntoken,
            onSendProgress: (received, total) =>
                onSendProgress(received, total, onProgress),
            queryParameters: _params,
            options: await _getRequestOptions(null, _params))
        .catchError(mapErrorResponse);
      
        // printBlue(response.data);
    ApiDataResponse<TType> res = ApiDataResponse<TType>.fromJson({"status":200,"data":response.data});
    if (response.statusCode == 401)
      throw ApiFetchException(
          this.authorizationMessage, ApiDataResponse.fromJson({"status": 401}));
    res.status = res.status ?? (res.data != null ? 200 : 400);
    if (response.statusCode == 200 && res.status == 200) {
      printGreen(response.data);
      if (res.data is Map<String, dynamic>) {
        await AuthHelper.instance
            .authenticateDevice(res.data as Map<String, dynamic>);
      }
      printBlue(res.data);
      printYellow(res);
      return res;
    } else {
      printRed(response.data);
      throw ApiFetchException(res.error, res);
    }
  }

  Future<ApiDataResponse> deleteObject(String url,
      {Map<String, String> params}) async {
    var _params = params ?? {};
    var response = await _getDioRequest()
        .delete<Map<String, dynamic>>(url,
            queryParameters: _params,
            options: await _getRequestOptions(null, _params))
        .catchError(mapErrorResponse);
    ApiDataResponse res = ApiDataResponse.fromJson(response.data);
    if (response.statusCode == 401)
      throw ApiFetchException(
          this.authorizationMessage, ApiDataResponse.fromJson({"status": 401}));
    res.status = res.status ?? (res.data != null ? 200 : 400);
    if (response.statusCode == 200 && res.status == 200) {
      printGreen(response.data);
      return res;
    } else {
      printRed(response.data);
      throw ApiFetchException(res.error, res);
    }
  }

  Future downloadFile(String url, String path,
      {Function(double) onProgress, Map<String, String> params}) async {
    var _params = params ?? {};
    var response = await _getDioRequest()
        .download(url, path,
            onReceiveProgress: (received, total) =>
                onSendProgress(received, total, onProgress),
            queryParameters: _params,
            options: await _getRequestOptions(null, _params))
        .catchError(mapErrorResponse);
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception("Cant download file Status ${response.statusCode}");
    }
  }
}
