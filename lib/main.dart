import 'package:demo/pages/Activation_acount/activation_code.dart';
import 'package:demo/pages/Activation_acount/phone_number.dart';
import 'package:demo/pages/forgotpassword/forgotpassword.dart';
import 'package:demo/pages/forgotpassword/message_confirm.dart';
import 'package:demo/pages/forgotpassword/resetpassword.dart';
import 'package:demo/pages/home/PageAirport.dart';
import 'package:demo/pages/home/PageCarForRent.dart';
import 'package:demo/pages/home/PageFilter.dart';
import 'package:demo/pages/home/PageOffers.dart';
import 'package:demo/pages/login/login.dart';
import 'package:demo/pages/map/arrival%20_loaction.dart';

import 'package:demo/pages/register/step_four.dart';
import 'package:demo/pages/register/step_one.dart';
import 'package:demo/pages/register/step_three.dart';
import 'package:demo/pages/register/step_two.dart';
import 'package:demo/pages/searchShare/PageTabBarView.dart';
import 'package:demo/pages/splash/splash.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

import 'Components/NavigationBar.dart';
import 'allTranslations.dart';

void main() async {
    // Initializes the translation module
    await allTranslations.init();

    // then start the application
    runApp( MyApp(),);
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
       
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);


  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
 
  @override
  Widget build(BuildContext context) {
  return new SplashScreen(
      seconds: 3,
      navigateAfterSeconds: Page_PhoneNumber(),
      imageBackground: AssetImage('asset/splash.png'),
      styleTextUnderTheLoader: new TextStyle(),
      onClick: ()=>print("Deserve"),
      loaderColor: Colors.white,
    );
  }
}




// import 'dart:async';
// import 'package:demo/Tools/print.dart';
// import 'package:flutter/material.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// //import 'package:flutter_google_places/flutter_google_places.dart';
// //import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:flutter_facebook_login/flutter_facebook_login.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// class _MyAppState extends State<MyApp> {


//   Completer<GoogleMapController> _controller = Completer();

//   static const LatLng _center = const LatLng(45.521563, -122.677433);

//   final Set<Marker> _markers = {};

//   LatLng _lastMapPosition = _center;

//   MapType _currentMapType = MapType.normal;

//   void _onMapTypeButtonPressed() {
//     setState(() {
//       _currentMapType = _currentMapType == MapType.normal
//           ? MapType.satellite
//           : MapType.normal;
//     });
//   }

//   void _onAddMarkerButtonPressed() {
//     setState(() {
//       _markers.add(Marker(
//         // This marker id can be anything that uniquely identifies each marker.
//         markerId: MarkerId(_lastMapPosition.toString()),
//         position: _lastMapPosition,
//         infoWindow: InfoWindow(
//           title: _lastMapPosition.toString(),
//           snippet: '5 Star Rating',
//         ),
//         icon: BitmapDescriptor.defaultMarker,
//       ));
//     });
//   }

//   void _onCameraMove(CameraPosition position) {
//     _lastMapPosition = position.target;
//   }

//   void _onMapCreated(GoogleMapController controller) {
//     _controller.complete(controller);
//   }
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text('Maps Sample App'),
//           backgroundColor: Colors.green[700],
//         ),
//         body: Stack(
//           children: <Widget>[
//             GoogleMap(
//               onMapCreated: _onMapCreated,
//               initialCameraPosition: CameraPosition(
//                 target: _center,
//                 zoom: 11.0,
//               ),
//               mapType: _currentMapType,
//               markers: _markers,
//               onCameraMove: _onCameraMove,
//             ),
//             Padding(
//               padding: const EdgeInsets.all(16.0),
//               child: Align(
//                 alignment: Alignment.topRight,
//                 child: Column(
//                   children: <Widget> [
//                     FloatingActionButton(
//                       onPressed: _onMapTypeButtonPressed,
//                       materialTapTargetSize: MaterialTapTargetSize.padded,
//                       backgroundColor: Colors.green,
//                       child: const Icon(Icons.map, size: 36.0),
//                     ),
//                     SizedBox(height: 16.0),
//                     FloatingActionButton(
//                       onPressed: _onAddMarkerButtonPressed,
//                       materialTapTargetSize: MaterialTapTargetSize.padded,
//                       backgroundColor: Colors.green,
//                       child: const Icon(Icons.add_location, size: 36.0),
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
