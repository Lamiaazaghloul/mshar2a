import 'package:demo/pages/home/HomePage.dart';
import 'package:demo/pages/home/PageAirport.dart';
import 'package:demo/pages/home/PageCarForRent.dart';
import 'package:demo/pages/home/PageFilter.dart';
import 'package:demo/pages/home/PageOffers.dart';
import 'package:demo/pages/home/messages.dart';
import 'package:flutter/material.dart';


import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class NavigationBar extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<NavigationBar> {

  int _pageIndex = 0;

  // هنا الصفحات التي سوف يتم التنقل بينهم عن طريق NavigationBar
  final HomePage _Home = new HomePage();
  final PageAirport _Airport = new PageAirport();
  final PageCarForRent _CarForRent = new PageCarForRent();
  final PageOffers _Offers = new PageOffers();
  final MyMessages _Filter = new MyMessages();

  Widget _showpage = new HomePage();
  Widget _pageChooser(int page){
    switch(page){
      case 4 : return _Offers ;
      break;

      case 3 : return _Filter ;
      break;

      case 2 : return _Airport ;
      break;

      case 1 : return _CarForRent ;
      break;

      case 0 : return _Home ;
      break;
    }
  }



  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:   Directionality(
                textDirection: TextDirection.rtl,
                child:new Scaffold(

        //====bottomNavigationBar===============================================
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(FontAwesomeIcons.home), title: Text('الرئيسية')),
            BottomNavigationBarItem(icon: Icon(FontAwesomeIcons.car), title: Text('طلب سيارة')),
            BottomNavigationBarItem(icon: Icon(Icons.local_car_wash), title: Text('الرحلات')),

            BottomNavigationBarItem(icon: Icon(Icons.email), title: Text('الرسائل')),

            BottomNavigationBarItem(icon: Icon(Icons.settings_applications), title: Text('العروض')),
          ],
          type: BottomNavigationBarType.fixed,      // لاظهار باقي العناصر التي تم اختفائها
          currentIndex: _pageIndex,
          elevation: 0,
          unselectedItemColor: Colors.blueGrey,
          backgroundColor: Colors.white,
          onTap: (int tappedIndex){
            setState(() {
              _showpage = _pageChooser(tappedIndex);
            });
          },
        ),




        //====body=========================================================
        body: Container(
          child: Center(
            child: _showpage,
          ),
        ),

      ),
    ));
  }
}