import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:flutter/material.dart';



Widget my_buildContainer1({
  String price = '100',
  String CarType = "كامري",
  String TimeStart = "13:10",
  String TimeEnd = "11my_buildContainer2:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String Number = "3 / 4" ,
  String DateStart = "16يناير" ,
  String DateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
  GestureTapCallback details
}) {
  return new GestureDetector(
    onTap: details,
    child:Container(
    height: 120,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

        //==================================السعر وحجز الان   =
        new Expanded(flex: 2,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("ريال" , style: TextStyle(fontSize: 10),),
                      Text("${price}" , style: TextStyle(fontSize: 17),),
                    ],
                  ),
//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "احجز",horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),

        //==================================البيانات الداخلية  ويوجد بها قسمين   =
        new Expanded(flex: 6,child:
        Container(child: Row(
          children: <Widget>[


            //================================== القسم الاثاني نوع السيارة وعدد الركاب  =
            Expanded(
                flex: 2,child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
//                        color: Colors.teal,
              child: Column(
                children: <Widget>[

                  //================================== نوع السيارة  =
                  Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                            border: BorderDirectional(bottom: BorderSide(color: Colors.grey[100] ,width: 1))
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text("${CarType}" ,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                            Icon(Icons.directions_car,size: 15,color: Colors.grey)
                          ],
                        ),)),

                  //================================== عدد الركاب  =
                  Expanded(
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text("${Number}" ,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                            Icon(Icons.directions_car,size: 15,color: Colors.grey)
                          ],
                        ),)),

                ],
              ),)
            ),


            //================================== القسم الاول من والي  / يسمع بأخذ الراكب من المنزل  =
            Expanded(
              flex: 6,child: Container(
              decoration: BoxDecoration(
              border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                children: <Widget>[

                //================================================ من الرياض الي جدة ===
                Padding(
                  padding: const EdgeInsets.only(left: 5 ,right: 5 , top: 5 ,bottom: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("${nameFrom}", style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                      Text("${nameTo}", style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                    ],
                  ),
                ),



                //==============================================الخط الي يظهر تحت المنطقة ==
                Expanded(
                  child: Stack(children: <Widget>[
                  //----- هذا الخد
                  Padding(
                    padding: const EdgeInsets.only(left: 5 , right: 5 ,top: 6),
                    child: new Container(height: 1 , color: colorActive,),
                  ),

                  //----- هذا دائرة اليسار
                  Positioned(left: 5,
                      child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                      child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                      child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                      )),

                  //----- هذا دائرة اليمين
                  Positioned(
                    right: 5,
                    child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                    child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                    child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                    )),

                ],)),


                Padding(
                  padding: const EdgeInsets.only(left: 4 , right: 4 ,bottom: 15),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[

                    new Column(children: <Widget>[
                      Stack(overflow: Overflow.visible,
                          children: <Widget>[
                            Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                            Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                          ])
                    ],),

                    new Column(children: <Widget>[
                      Stack(overflow: Overflow.visible,
                          children: <Widget>[
                            Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                            Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                          ])
                    ],),


                  ],),
                ),


                //============================================== يسمح بأخد الراكب من المنزل ==
                Padding(
                  padding: const EdgeInsets.only(left: 0 , right: 0,bottom: 7 ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(width: 3),
                      Text("يسمح بأخد الراكب من المنزل", style: TextStyle(fontSize: 9,color: Colors.grey),),
                      Icon(Icons.home,size: 12,color: Colors.grey,),
                    ],
                  ),
                ),



              ],),
            )
            ),

          ],
        ),)),

        //==================================صورة العميل  =
        new Expanded(flex: 2, child: _CircleImage(imageUrl , colorActiveImage: colorActive)),
      ],
    ),
  ));
  }



Widget my_buildContainer2({
  String price = '100',
  String CarType = "كامري",
  String TimeStart = "13:10",
  String TimeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String Number = "3 / 4" ,
  String DateStart = "16يناير" ,
  String DateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
//    padding: EdgeInsets.only(top: 25),
    height: 120,
//    margin: EdgeInsets.only(top: 10,left: 4,right: 4),
     margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

        //==================================السعر وحجز الان   =
        new Expanded(flex: 2,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Container(padding: EdgeInsets.symmetric(vertical: 3),
                    decoration: BoxDecoration(
                        border: BorderDirectional(bottom: BorderSide(color: Colors.grey[100] ,width: 1))
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("${CarType}" ,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                        Icon(Icons.directions_car,size: 15,color: Colors.grey)
                      ],
                    )),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("ريال" , style: TextStyle(fontSize: 10),),
                      Text("${price}" , style: TextStyle(fontSize: 17),),
                    ],
                  ),
//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "احجز",horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),

        //==================================البيانات الداخلية  ويوجد بها قسمين   =
        new Expanded(flex: 6,child:
        Container(child: Row(
          children: <Widget>[
            //================================== القسم الاول من والي  / يسمع بأخذ الراكب من المنزل  =
            Expanded(
              flex: 6,child: Container(
              decoration: BoxDecoration(
              border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                children: <Widget>[

                //================================================ من الرياض الي جدة ===
                Padding(
                  padding: const EdgeInsets.only(left: 5 ,right: 5 , top: 3 ,bottom: 3),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("${nameFrom}", style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                      Text("${nameTo}", style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                    ],
                  ),
                ),



                //==============================================الخط الي يظهر تحت المنطقة ==
                Expanded(
                  child: Stack(children: <Widget>[
                  //----- هذا الخد
                  Padding(
                    padding: const EdgeInsets.only(left: 5 , right: 5 ,top: 6),
                    child: new Container(height: 1 , color: colorActive,),
                  ),

                  //----- هذا دائرة اليسار
                  Positioned(left: 5,
                      child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                      child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                      child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                      )),

                  //----- هذا دائرة اليمين
                  Positioned(
                    right: 5,
                    child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                    child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                    child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                    )),

                  //----- هذا دائرة الوسط
                  Positioned(
                    right: 5, left: 5,
                    child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                    child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                    child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                    )),

                ],)),

                //=============================== وقت القلاع والوصول وتاريخ الوصول =
                Padding(
                  padding: const EdgeInsets.only(left: 4 , right: 4 ,bottom: 17),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                    new Column(children: <Widget>[
                        Stack(overflow: Overflow.visible,
                            children: <Widget>[
                              Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                              Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                            ])
                      ],),

                    new Column(children: <Widget>[
                        Stack(overflow: Overflow.visible,
                            children: <Widget>[
                              Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                              Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                            ])
                      ],),

                    new Column(children: <Widget>[
                        Stack(overflow: Overflow.visible,
                            children: <Widget>[
                              Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                              Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                            ])
                      ],),

                    ],),
                ),

                //============================================== يسمح بأخد الراكب من المنزل و عدد الركاب ==
                Padding(
                  padding: const EdgeInsets.only(left: 0 , right: 0,bottom: 13 ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(Number, style: TextStyle(fontSize: 9,color: Colors.grey),),
                      Icon(Icons.directions_car,size: 12,color: Colors.grey,),

                      SizedBox(width: 10,),
                      Text("يسمح بأخد الراكب من المنزل", style: TextStyle(fontSize: 9,color: Colors.grey),),
                      Icon(Icons.home,size: 12,color: Colors.grey,),
                    ],
                  ),
                ),



              ],),
            )
            ),

          ],
        ),)),

        //==================================صورة العميل  =
        new Expanded(flex: 2, child: _CircleImage(imageUrl , colorActiveImage: colorActive)),
      ],
    ),
  );
}





Widget my_buildContainer3({
  String price = '100',
  String CarType = "كامري",
  String TimeStart = "13:10",
  String TimeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String Number = "3 / 4" ,
  String DateStart = "16يناير" ,
  String DateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 140,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

        //==================================السعر وحجز الان   =
        new Expanded(flex: 2,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Container(padding: EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                          border: BorderDirectional(bottom: BorderSide(color: Colors.grey[100] ,width: 1))
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("${CarType}" ,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                          Icon(Icons.directions_car,size: 15,color: Colors.grey)
                        ],
                      )),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("ريال" , style: TextStyle(fontSize: 10),),
                      Text("${price}" , style: TextStyle(fontSize: 17),),
                    ],
                  ),
//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "احجز",horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),

        //==================================ممنوع التدخين و عدد الركاب ويسمح باخذ الراكب من المنزل =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("يسمح بأخذ الراكب من المنزل" ,style: TextStyle(fontSize:7 ,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    )
                  ],
                ),
                new Divider(),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("ممنوع التدخين " ,style: TextStyle(fontSize: 7 ,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    )
                  ],
                ),

                new Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("${Number}" ,style: TextStyle(fontSize: 7 ,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    )
                  ],
                ),


              ],),
            )
        ),

        //================================================== مكان الاقلاع والوقت والتاريخ ===
        new Expanded(flex: 1,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container( height: 40,
                    child: Center(child: Stack(overflow: Overflow.visible,
                          children: <Widget>[
                          Positioned(left: 4,top: -8,child: Text("${nameFrom}", style: TextStyle(fontSize: 8,color: Colors.grey),)),
                          Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                          Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                        ]))),

                    new Container( height: 40,
                    child: Center(child: Stack(overflow: Overflow.visible,
                          children: <Widget>[
                          Positioned(left: 4,top: -8,child: Text("${nameFrom}", style: TextStyle(fontSize: 8,color: Colors.grey),)),
                          Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                          Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                        ]))),


                    new Container( height: 40,
                    child: Center(child: Stack(overflow: Overflow.visible,
                          children: <Widget>[
                          Positioned(left: 4,top: -8,child: Text("${nameFrom}", style: TextStyle(fontSize: 8,color: Colors.grey),)),
                          Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                          Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                        ]))),
                ],
              ),
            )
        ),

        //================================================== الخط ونقاط التوصيل  ===
        new Expanded(flex: 1,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                children: <Widget>[


                  //==============================================الخط الي يظهر تحت المنطقة ==
                  Expanded(
                      child: Stack(children: <Widget>[
                        //----- هذا الخط
                        Padding(
                          padding: const EdgeInsets.only(left:10 , right: 10 ,top: 10 , bottom: 10),
                          child: new Container(width: 1 , color: colorActive,),
                        ),

                        //----- هذا دائرة اليسار
                        Positioned(top: 8, right: 1,left: 1,
                            child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                              child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                                child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                            )),

                        //----- هذا دائرة اليمين
                        Positioned(
                            bottom: 5,top: 5,left: 3,
                            child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                              child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                                child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                            )),

                        //----- هذا دائرة اليمين
                        Positioned(
                            bottom: 8,right: 0,left: 1,
                            child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                            child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                            child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                            )),
                      ],)),


                ],
              ),
            )
        ),

        //==================================صورة العميل  =
        new Expanded(flex: 2, child: _CircleImage(imageUrl , colorActiveImage: colorActive)),
      ],
    ),
  );
}


Widget my_buildContainer31({
  String price = '100',
  String CarType = "كامري",
  String TimeStart = "13:10",
  String TimeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String Number = "3 / 4" ,
  String DateStart = "16يناير" ,
  String DateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 120,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

        //==================================السعر وحجز الان   =
        new Expanded(flex: 2,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Container(padding: EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                          border: BorderDirectional(bottom: BorderSide(color: Colors.grey[100] ,width: 1))
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("${CarType}" ,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                          Icon(Icons.directions_car,size: 15,color: Colors.grey)
                        ],
                      )),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("ريال" , style: TextStyle(fontSize: 10),),
                      Text("${price}" , style: TextStyle(fontSize: 17),),
                    ],
                  ),
//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "احجز",horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),

        //==================================ممنوع التدخين و عدد الركاب ويسمح باخذ الراكب من المنزل =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("مكيف" ,style: TextStyle(fontSize:7 ,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    )
                  ],
                ),
                new Divider(),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("ممنوع التدخين " ,style: TextStyle(fontSize: 7 ,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    )
                  ],
                ),

                new Divider(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("${Number}" ,style: TextStyle(fontSize: 7 ,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    )
                  ],
                ),


              ],),
            )
        ),

        //================================================== مكان الاقلاع والوقت والتاريخ ===
        new Expanded(flex: 1,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Container( height: 40,
                    child: Center(child: Stack(overflow: Overflow.visible,
                          children: <Widget>[
                          Positioned(left: 4,top: -8,child: Text("${nameFrom}", style: TextStyle(fontSize: 8,color: Colors.grey),)),
                          Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                          Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                        ]))),

                    // new Container( height: 40,
                    // child: Center(child: Stack(overflow: Overflow.visible,
                    //       children: <Widget>[
                    //       Positioned(left: 4,top: -8,child: Text("${nameFrom}", style: TextStyle(fontSize: 8,color: Colors.grey),)),
                    //       Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                    //       Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                    //     ]))),


                    new Container( height: 40,
                    child: Center(child: Stack(overflow: Overflow.visible,
                          children: <Widget>[
                          Positioned(left: 4,top: -8,child: Text("${nameFrom}", style: TextStyle(fontSize: 8,color: Colors.grey),)),
                          Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                          Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                        ]))),
                ],
              ),
            )
        ),

        //================================================== الخط ونقاط التوصيل  ===
        new Expanded(flex: 1,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                children: <Widget>[


                  //==============================================الخط الي يظهر تحت المنطقة ==
                  Expanded(
                      child: Stack(children: <Widget>[
                        //----- هذا الخط
                        Padding(
                          padding: const EdgeInsets.only(left:10 , right: 10 ,top: 10 , bottom: 10),
                          child: new Container(width: 1 , color: colorActive,),
                        ),

                        //----- هذا دائرة اليسار
                        Positioned(top: 8, right: 1,left: 1,
                            child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                              child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                                child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                            )),

                        //----- هذا دائرة اليمين
                        // Positioned(
                        //     bottom: 5,top: 5,left: 3,
                        //     child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                        //       child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                        //         child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                        //     )),

                        //----- هذا دائرة اليمين
                        Positioned(
                            bottom: 8,right: 0,left: 1,
                            child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                            child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                            child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                            )),
                      ],)),


                ],
              ),
            )
        ),

        //==================================صورة العميل  =
        new Expanded(flex: 2, child: _CircleImage(imageUrl , colorActiveImage: colorActive)),
      ],
    ),
  );
}

//message list 

Widget my_Message_List({
  String price = '100',
  String CarType = "كامري",
  String TimeStart = "13:10",
  String TimeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String Number = "3 / 4" ,
  String DateStart = "16يناير" ,
  String DateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 140,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

        //==================================السعر وحجز الان   =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  // Container(padding: EdgeInsets.symmetric(vertical: 3),
                  //     decoration: BoxDecoration(
                  //         border: BorderDirectional(bottom: BorderSide(color: Colors.grey[100] ,width: 1))
                  //     ),
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.center,
                  //       crossAxisAlignment: CrossAxisAlignment.center,
                  //       children: <Widget>[
                  //         Text("${CarType}" ,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                  //         Icon(Icons.directions_car,size: 15,color: Colors.grey)
                  //       ],
                  //     )),

                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.center,
                  //   children: <Widget>[
                  //     Text("ريال" , style: TextStyle(fontSize: 10),),
                  //     Text("${price}" , style: TextStyle(fontSize: 17),),
                  //   ],
                  // ),
//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "مراسلة",horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),

        //==================================ممنوع التدخين و عدد الركاب ويسمح باخذ الراكب من المنزل =
        // new Expanded(flex: 5,
        //     child: Container(
        //       decoration: BoxDecoration(
        //           border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
        //       ),
        //       child: Column(
        //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //         crossAxisAlignment: CrossAxisAlignment.center,
        //         children: <Widget>[

        //         Row(
        //           mainAxisAlignment: MainAxisAlignment.end,
        //           crossAxisAlignment: CrossAxisAlignment.center,
        //           children: <Widget>[
        //             Text("مكيف" ,style: TextStyle(fontSize:10,color: Colors.grey),),
        //             Padding(
        //               padding: const EdgeInsets.only(right: 5,left: 5),
        //               child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
        //             )
        //           ],
        //         ),
        //         new Divider(),

        //         Row(
        //           mainAxisAlignment: MainAxisAlignment.end,
        //           crossAxisAlignment: CrossAxisAlignment.center,
        //           children: <Widget>[
        //             Text("ممنوع التدخين " ,style: TextStyle(fontSize:10,color: Colors.grey),),
        //             Padding(
        //               padding: const EdgeInsets.only(right: 5,left: 5),
        //               child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
        //             )
        //           ],
        //         ),
        //         new Divider(),

        //         Row(
        //           mainAxisAlignment: MainAxisAlignment.end,
        //           crossAxisAlignment: CrossAxisAlignment.center,
        //           children: <Widget>[
        //             Text("اخد الراكب من المنزل" ,style: TextStyle(fontSize:12,color: Colors.grey),),
        //             Padding(
        //               padding: const EdgeInsets.only(right: 5,left: 5),
        //               child: Icon(Icons.directions_car,size: 15,color: Colors.grey),
        //             )
        //           ],
        //         ),


        //       ],),
        //     )
        // ),

        //================================================== بينات السائق ونوع السيارة  ===
        new Expanded(flex: 3,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[

                  new Text("كيا سيراتو" , style: TextStyle(fontSize: 15),),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("محمد جمال" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.account_circle,size: 11,color: Colors.grey),
                      )
                    ],
                  ),


                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("ذكر" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.assignment_ind,size: 11,color: Colors.grey),
                      )
                    ],
                  ),



                ],
              ),
            )
        ),


        //==================================صورة العميل  =
        new Expanded(flex: 3, child: _CircleImage(imageUrl , colorActiveImage: colorActive)),
      ],
    ),
  );
}

Widget my_buildContainer4({
  String price = '100',
  String CarType = "كامري",
  String TimeStart = "13:10",
  String TimeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String Number = "3 / 4" ,
  String DateStart = "16يناير" ,
  String DateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 140,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

        //==================================السعر وحجز الان   =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Container(padding: EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                          border: BorderDirectional(bottom: BorderSide(color: Colors.grey[100] ,width: 1))
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("${CarType}" ,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                          Icon(Icons.directions_car,size: 15,color: Colors.grey)
                        ],
                      )),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("ريال" , style: TextStyle(fontSize: 10),),
                      Text("${price}" , style: TextStyle(fontSize: 17),),
                    ],
                  ),
//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "احجز",horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),

        //==================================ممنوع التدخين و عدد الركاب ويسمح باخذ الراكب من المنزل =
        new Expanded(flex: 5,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("مكيف" ,style: TextStyle(fontSize:10,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    )
                  ],
                ),
                new Divider(),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("ممنوع التدخين " ,style: TextStyle(fontSize:10,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    )
                  ],
                ),
                new Divider(),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("اخد الراكب من المنزل" ,style: TextStyle(fontSize:12,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 15,color: Colors.grey),
                    )
                  ],
                ),


              ],),
            )
        ),

        //================================================== بينات السائق ونوع السيارة  ===
        new Expanded(flex: 3,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[

                  new Text("كيا سيراتو" , style: TextStyle(fontSize: 15),),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("محمد جمال" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.account_circle,size: 11,color: Colors.grey),
                      )
                    ],
                  ),


                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("ذكر" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.assignment_ind,size: 11,color: Colors.grey),
                      )
                    ],
                  ),



                ],
              ),
            )
        ),


        //==================================صورة العميل  =
        new Expanded(flex: 3, child: _CircleImage(imageUrl , colorActiveImage: colorActive)),
      ],
    ),
  );
}




Widget my_buildContainer5({
  String price = '100',
  String CarType = "كامري",
  String TimeStart = "13:10",
  String TimeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String Number = "3 / 4" ,
  String DateStart = "16يناير" ,
  String DateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 100,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

        //==================================السعر وحجز الان   =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  // Container(padding: EdgeInsets.symmetric(vertical: 3),
                  //     decoration: BoxDecoration(
                  //         border: BorderDirectional(bottom: BorderSide(color: Colors.grey[100] ,width: 1))
                  //     ),
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.center,
                  //       crossAxisAlignment: CrossAxisAlignment.center,
                  //       children: <Widget>[
                  //         Text("${CarType}" ,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                  //         Icon(Icons.directions_car,size: 15,color: Colors.grey)
                  //       ],
                  //     )),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("ريال" , style: TextStyle(fontSize: 10),),
                      Text("${price}" , style: TextStyle(fontSize: 17),),
                    ],
                  ),
//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "احجز",horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),

        //==================================ممنوع التدخين و عدد الركاب ويسمح باخذ الراكب من المنزل =
        // new Expanded(flex: 5,
        //     child: Container(
              
        //       decoration: BoxDecoration(
        //           border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
        //       ),
        //       child: Column(
        //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //         crossAxisAlignment: CrossAxisAlignment.center,
        //         children: <Widget>[

        //         Row(
        //           mainAxisAlignment: MainAxisAlignment.end,
        //           crossAxisAlignment: CrossAxisAlignment.center,
        //           children: <Widget>[
        //             Text("مكيف" ,style: TextStyle(fontSize:10,color: Colors.grey),),
        //             Padding(
        //               padding: const EdgeInsets.only(right: 5,left: 5),
        //               child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
        //             )
        //           ],
        //         ),
        //         new Divider(),

        //         Row(
        //           mainAxisAlignment: MainAxisAlignment.end,
        //           crossAxisAlignment: CrossAxisAlignment.center,
        //           children: <Widget>[
        //             Text("ممنوع التدخين " ,style: TextStyle(fontSize:10,color: Colors.grey),),
        //             Padding(
        //               padding: const EdgeInsets.only(right: 5,left: 5),
        //               child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
        //             )
        //           ],
        //         ),
        //         new Divider(),

        //         Row(
        //           mainAxisAlignment: MainAxisAlignment.end,
        //           crossAxisAlignment: CrossAxisAlignment.center,
        //           children: <Widget>[
        //             Text("اخد الراكب من المنزل" ,style: TextStyle(fontSize:12,color: Colors.grey),),
        //             Padding(
        //               padding: const EdgeInsets.only(right: 5,left: 5),
        //               child: Icon(Icons.directions_car,size: 15,color: Colors.grey),
        //             )
        //           ],
        //         ),


        //       ],),
        //     )
        // ),

        //================================================== بينات السائق ونوع السيارة  ===
        new Expanded(flex: 5,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[

                  new Text("كيا سيراتو" , style: TextStyle(fontSize: 15),),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("البداري - اسيوط - مصر" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.location_on,size: 18,color: Colors.grey),
                      )
                    ],
                  ),


                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("خصم 3% لمده يوم" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.monetization_on,size: 18,color: Colors.grey),
                      )
                    ],
                  ),



                ],
              ),
            )
        ),


        //==================================صورة العميل  =
        new Expanded(flex: 3, child: _CircleImage(imageUrl , colorActiveImage: colorActive)),
      ],
    ),
  );
}



Widget my_buildContainer6({
  String price = '100',
  String CarType = "كامري",
  String TimeStart = "13:10",
  String TimeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String Number = "3 / 4" ,
  String DateStart = "16يناير" ,
  String DateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 100,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

        //==================================السعر وحجز الان   =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  // Container(padding: EdgeInsets.symmetric(vertical: 3),
                  //     decoration: BoxDecoration(
                  //         border: BorderDirectional(bottom: BorderSide(color: Colors.grey[100] ,width: 1))
                  //     ),
                  //     child: Row(
                  //       mainAxisAlignment: MainAxisAlignment.center,
                  //       crossAxisAlignment: CrossAxisAlignment.center,
                  //       children: <Widget>[
                  //         Text("${CarType}" ,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                  //         Icon(Icons.directions_car,size: 15,color: Colors.grey)
                  //       ],
                  //     )),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("ريال" , style: TextStyle(fontSize: 10),),
                      Text("${price}" , style: TextStyle(fontSize: 17),),
                    ],
                  ),
//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "احجز",horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),

        //==================================ممنوع التدخين و عدد الركاب ويسمح باخذ الراكب من المنزل =
        // new Expanded(flex: 5,
        //     child: Container(
              
        //       decoration: BoxDecoration(
        //           border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
        //       ),
        //       child: Column(
        //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //         crossAxisAlignment: CrossAxisAlignment.center,
        //         children: <Widget>[

        //         Row(
        //           mainAxisAlignment: MainAxisAlignment.end,
        //           crossAxisAlignment: CrossAxisAlignment.center,
        //           children: <Widget>[
        //             Text("مكيف" ,style: TextStyle(fontSize:10,color: Colors.grey),),
        //             Padding(
        //               padding: const EdgeInsets.only(right: 5,left: 5),
        //               child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
        //             )
        //           ],
        //         ),
        //         new Divider(),

        //         Row(
        //           mainAxisAlignment: MainAxisAlignment.end,
        //           crossAxisAlignment: CrossAxisAlignment.center,
        //           children: <Widget>[
        //             Text("ممنوع التدخين " ,style: TextStyle(fontSize:10,color: Colors.grey),),
        //             Padding(
        //               padding: const EdgeInsets.only(right: 5,left: 5),
        //               child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
        //             )
        //           ],
        //         ),
        //         new Divider(),

        //         Row(
        //           mainAxisAlignment: MainAxisAlignment.end,
        //           crossAxisAlignment: CrossAxisAlignment.center,
        //           children: <Widget>[
        //             Text("اخد الراكب من المنزل" ,style: TextStyle(fontSize:12,color: Colors.grey),),
        //             Padding(
        //               padding: const EdgeInsets.only(right: 5,left: 5),
        //               child: Icon(Icons.directions_car,size: 15,color: Colors.grey),
        //             )
        //           ],
        //         ),


        //       ],),
        //     )
        // ),

        //================================================== بينات السائق ونوع السيارة  ===
        new Expanded(flex: 5,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[

                  new Text("كيا سيراتو" , style: TextStyle(fontSize: 15),),
 Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("محمد رزق" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.person_pin,size: 18,color: Colors.black54),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("البداري - اسيوط - مصر" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.location_on,size: 18,color: Colors.black54),
                      )
                    ],
                  ),


                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("خصم 3% لمده يوم" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.monetization_on,size: 18,color: Colors.grey),
                      )
                    ],
                  ),



                ],
              ),
            )
        ),


        //==================================صورة العميل  =
        new Expanded(flex: 3, child: _CircleImage(imageUrl , colorActiveImage: colorActive)),
      ],
    ),
  );
}







Widget my_buildContainer7({
  String price = '100',
  String CarType = "كامري",
  String TimeStart = "13:10",
  String TimeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String Number = "3 / 4" ,
  String DateStart = "16يناير" ,
  String DateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 140,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

        //==================================السعر وحجز الان   =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Container(padding: EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                          border: BorderDirectional(bottom: BorderSide(color: Colors.grey[100] ,width: 1))
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text("${CarType}" ,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                          Icon(Icons.directions_car,size: 15,color: Colors.grey)
                        ],
                      )),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("ريال" , style: TextStyle(fontSize: 10),),
                      Text("${price}" , style: TextStyle(fontSize: 17),),
                    ],
                  ),
//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "احجز",horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),

        //==================================ممنوع التدخين و عدد الركاب ويسمح باخذ الراكب من المنزل =
        new Expanded(flex: 5,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("مكيف" ,style: TextStyle(fontSize:10,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    )
                  ],
                ),
                new Divider(),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("ممنوع التدخين " ,style: TextStyle(fontSize:10,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    )
                  ],
                ),
                new Divider(),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("اخد الراكب من المنزل" ,style: TextStyle(fontSize:12,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 15,color: Colors.grey),
                    )
                  ],
                ),


              ],),
            )
        ),

        //================================================== بينات السائق ونوع السيارة  ===
        new Expanded(flex: 3,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[

                  new Text("كيا سيراتو" , style: TextStyle(fontSize: 15),),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("محمد جمال" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.account_circle,size: 11,color: Colors.grey),
                      )
                    ],
                  ),


                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("ذكر" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.assignment_ind,size: 11,color: Colors.grey),
                      )
                    ],
                  ),



                ],
              ),
            )
        ),


        //==================================صورة العميل  =
        new Expanded(flex: 3, child: _CircleImage(imageUrl , colorActiveImage: colorActive)),
      ],
    ),
  );
}



Widget my_buildContainer44({
  String price = '100',
  String CarType = "كامري",
  String TimeStart = "13:10",
  String TimeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String Number = "3 / 4" ,
  String DateStart = "16يناير" ,
  String DateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  Color colorBG = Colors.white,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 140,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
        color: colorBG,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

        //==================================السعر وحجز الان   =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Container(padding: EdgeInsets.symmetric(vertical: 3),
                      decoration: BoxDecoration(
                          border: BorderDirectional(bottom: BorderSide(color: Colors.grey[100] ,width: 1))
                      ),
                      // child: Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   crossAxisAlignment: CrossAxisAlignment.center,
                      //   children: <Widget>[
                      //     Text("${CarType}" ,style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                      //     Icon(Icons.directions_car,size: 15,color: Colors.grey)
                      //   ],
                      // )
                      ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("ريال" , style: TextStyle(fontSize: 10),),
                      Text("${price}" , style: TextStyle(fontSize: 17),),
                    ],
                  ),
//                          Text("ريال", style: TextStyle(fontSize: 10),),
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "احجز",horizontal: 5 , radiusButton: 5 ,heightButton: 30,onBtnclicked: onTap)
                ],
              ),
            )),

        //==================================ممنوع التدخين و عدد الركاب ويسمح باخذ الراكب من المنزل =
        new Expanded(flex: 5,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("مكيف" ,style: TextStyle(fontSize:10,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    )
                  ],
                ),
                new Divider(),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("ممنوع التدخين " ,style: TextStyle(fontSize:10,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 11,color: Colors.grey),
                    )
                  ],
                ),
                new Divider(),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("اخد الراكب من المنزل" ,style: TextStyle(fontSize:12,color: Colors.grey),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.directions_car,size: 15,color: Colors.grey),
                    )
                  ],
                ),


              ],),
            )
        ),

        //================================================== بينات السائق ونوع السيارة  ===
        new Expanded(flex: 3,
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[

                  new Text("محمد جمال" , style: TextStyle(fontSize: 15),),

                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.end,
                  //   crossAxisAlignment: CrossAxisAlignment.center,
                  //   children: <Widget>[
                  //     Text("محمد جمال" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                  //     Padding(
                  //       padding: const EdgeInsets.only(left: 5),
                  //       child: Icon(Icons.account_circle,size: 11,color: Colors.grey),
                  //     )
                  //   ],
                  // ),


                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text("ذكر" ,style: TextStyle(fontSize:10 ,color: Colors.grey),),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: Icon(Icons.assignment_ind,size: 11,color: Colors.grey),
                      )
                    ],
                  ),



                ],
              ),
            )
        ),


        //==================================صورة العميل  =
        new Expanded(flex: 3, child: _CircleImage(imageUrl , colorActiveImage: colorActive)),
      ],
    ),
  );
}




Widget my_buildContainer55({
  String price = '100',
  String CarType = "كامري",
  String TimeStart = "13:10",
  String TimeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String Number = "3 / 4" ,
  String DateStart = "16يناير" ,
  String DateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    height: 120,
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(8),
    ),
    child: Row(
      children: <Widget>[

        //==================================البيانات الداخلية  ويوجد بها قسمين   =
        new Expanded(flex: 6,child:
        Container(child: Row(
          children: <Widget>[

            //================================== القسم الاول من والي  / يسمع بأخذ الراكب من المنزل  =
            Expanded(
                flex: 6,child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                children: <Widget>[

                  //================================================ من الرياض الي جدة ===
                  Padding(
                    padding: const EdgeInsets.only(left: 5 ,right: 5 , top: 5 ,bottom: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("${nameFrom}", style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                        Text("${nameTo}", style: TextStyle(fontSize: 10 ,color: Colors.grey),),
                      ],
                    ),
                  ),



                  //==============================================الخط الي يظهر تحت المنطقة ==
                  Expanded(
                      child: Stack(children: <Widget>[
                        //----- هذا الخد
                        Padding(
                          padding: const EdgeInsets.only(left: 5 , right: 5 ,top: 6),
                          child: new Container(height: 1 , color: colorActive,),
                        ),

                        //----- هذا دائرة اليسار
                        Positioned(left: 5,
                            child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                              child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                                child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                            )),

                        //----- هذا دائرة اليمين
                        Positioned(
                            right: 5,
                            child:new CircleAvatar(maxRadius: 7, backgroundColor: colorActive,
                              child:CircleAvatar(maxRadius:  6,backgroundColor: Colors.white,
                                child: CircleAvatar(maxRadius: 4,backgroundColor: colorActive,),),
                            )),

                      ],)),


                  Padding(
                    padding: const EdgeInsets.only(left: 4 , right: 4 ,bottom: 15),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[

                        new Column(children: <Widget>[
                          Stack(overflow: Overflow.visible,
                              children: <Widget>[
                                Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                                Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                              ])
                        ],),

                        new Column(children: <Widget>[
                          Stack(overflow: Overflow.visible,
                              children: <Widget>[
                                Positioned(left: 4,bottom: -8,child: Text("${DateEnd}", style: TextStyle(fontSize: 8 ,color: Colors.grey),)),
                                Text("${TimeStart}", style: TextStyle(fontSize: 13 ,fontWeight: FontWeight.bold,color: Colors.grey),),
                              ])
                        ],),


                      ],),
                  ),


                  //============================================== يسمح بأخد الراكب من المنزل ==
                  Padding(
                    padding: const EdgeInsets.only(left: 0 , right: 0,bottom: 7 ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(width: 3),



                        SizedBox(width: 20),
                        Text("ممنوع التدخين", style: TextStyle(fontSize: 9,color: Colors.grey),),
                        Icon(Icons.smoke_free,size: 12,color: Colors.grey,),

                        SizedBox(width: 20),
                        Text("3 / 4", style: TextStyle(fontSize: 9,color: Colors.grey),),
                        Icon(Icons.directions_car,size: 12,color: Colors.grey,),

                        SizedBox(width: 20),
                        Text("كيا سيراتو", style: TextStyle(fontSize: 9,color: Colors.grey),),
                        Icon(Icons.directions_car,size: 12,color: Colors.grey,),

                        SizedBox(width: 20),
                        Text("يسمح بأخد الراكب من المنزل", style: TextStyle(fontSize: 9,color: Colors.grey),),
                        Icon(Icons.home,size: 12,color: Colors.grey,),


                      ],
                    ),
                  ),



                ],),
            )
            ),

          ],
        ),)),

      ],
    ),
  );
}



Widget my_buildContaine({
  String price = '100',
  String CarType = "كامري",
  String TimeStart = "13:10",
  String TimeEnd = "11:10",
  String nameFrom = "الرياض" ,
  String nameTo = "جدة" ,
  String Number = "3 / 4" ,
  String DateStart = "16يناير" ,
  String DateEnd = "16يناير" ,
  String imageUrl = "",
  Color colorBG = Colors.white,
  Color colorActive = Colors.green,
  VoidCallback onTap ,
}) {
  return new Container(
    margin: EdgeInsets.symmetric(horizontal: 8,vertical: 5),
    decoration: BoxDecoration(
    color: colorBG,
    borderRadius: BorderRadius.circular(8),
    boxShadow: [new BoxShadow(color: Colors.grey[200], blurRadius: 5.0,),]
    ),
    child: Row(
      children: <Widget>[

        //==================================السعر وحجز الان   =
        new Expanded(flex: 3,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "متابعة ",horizontal: 5 , radiusButton: 5 ,heightButton: 20,onBtnclicked: onTap),
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "ارسال رسالة ",horizontal: 5 , radiusButton: 5 ,heightButton: 20,onBtnclicked: onTap),
                  my_Button(fontSize: 12,colorButton: anCyan,textButton: "تبليغ ",horizontal: 5 , radiusButton: 5 ,heightButton: 20,onBtnclicked: onTap),
                ],
              ),
            )),

        //==================================بيانات السائق =
        new Expanded(flex: 5,
            child: Container(
              decoration: BoxDecoration(
                  border: BorderDirectional(end: BorderSide(color: Colors.grey[100] ,width: 1))
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("محمد جمال" ,style: TextStyle(fontSize:14,color: Colors.grey[600]),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.account_circle,size: 18,color: Colors.grey[800]),
                    )
                  ],
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("ذكر" ,style: TextStyle(fontSize:14,color: Colors.grey[600]),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.account_circle,size: 18,color: Colors.grey[800]),
                    )
                  ],
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text("جدة" ,style: TextStyle(fontSize:14,color: Colors.grey[600]),),
                    Padding(
                      padding: const EdgeInsets.only(right: 5,left: 5),
                      child: Icon(Icons.location_on,size: 18,color: Colors.grey[700]),
                    )
                  ],
                ),


                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Icon(Icons.star , size: 18, color: Colors.amber,),
                    new Icon(Icons.star , size: 18, color: Colors.amber,),
                    new Icon(Icons.star , size: 18, color: Colors.amber,),
                    new Icon(Icons.star , size: 18, color: Colors.amber,),
                    new Icon(Icons.star , size: 18, color: Colors.amber,),
                  ],
                ),


              ],),
            )
        ),


        //==================================صورة العميل  =
        new Expanded(flex: 3, child: _CircleImage(imageUrl , colorActiveImage: colorActive)),
      ],
    ),
  );
}

Widget _CircleImage(String image ,{Color colorActiveImage = Colors.green}) {
  return Container(
    child: Center(child:
    Stack(children: <Widget>[
      new Container(
        width: 65,
        height: 65,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: Colors.white , width: 3),
            image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(image))
        ),
      ),
      Positioned(
          bottom: 0 , left: 10,
          child: new CircleAvatar(maxRadius: 7, backgroundColor: Colors.white,
            child:CircleAvatar(maxRadius: 5,backgroundColor: colorActiveImage,),
          ))
    ],
    ),
    ),
  );
}



