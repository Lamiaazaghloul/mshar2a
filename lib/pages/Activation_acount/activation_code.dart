
import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:demo/pages/login/login.dart';
import 'package:flutter/material.dart';



class Page_ActivationCode extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<Page_ActivationCode> {


  //  ============== ============================اكشن التفعيل
  void _FunctionButtonActive(){
    print("متابعة ");
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Login()),
  );
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),

      home:  new Scaffold(
        backgroundColor: anCyan,

        body: ListView(
          children: <Widget>[

            // image Logo==========================================
            new Container(
              padding: EdgeInsets.only(top: 130 , left: 50 , right: 50),
              child: Image.asset("asset/Logo1.png",height: 85,),
            ),

            // text phone Number ======================================
            SizedBox(height: 30,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50),
              child: Align( alignment: Alignment.center,
                  child: new Text("ادخل رمز التفعيل الذي تم ارسالة الي رقم هاتفك " ,
                    style: TextStyle(fontSize: 20 , color: anwhite ),textAlign: TextAlign.center,)
              ),
            ),

            // text TextField =========================================
            SizedBox(height: 30,),
            Directionality(textDirection: TextDirection.rtl,
                child: my_TextFieldMaterial(
                    Radius: 1 ,horizontal: 50 , elevation: 0,textInputType: TextInputType.number ,
                    prefixIcon: Icons.done ,HintText: "رمز التفعيل")
            ),


            // text TextField =========================================
            SizedBox(height: 30,),
            my_Button(
                onBtnclicked: (){_FunctionButtonActive();} ,
                horizontal: 120 , radiusButton: 25 , colorButton: anwhite , colorText: Colors.grey,
                textButton: "تفعيل" , fontSize: 20
            )

          ],
        ),


      ),
    );
  }
}