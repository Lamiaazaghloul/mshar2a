
import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:demo/pages/Activation_acount/activation_code.dart';
import 'package:flutter/material.dart';



class Page_PhoneNumber extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<Page_PhoneNumber> {


  //  ============== ============================اكشن المتابعة
  void _FunctionButtonNext(){
    print("متابعة ");
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Page_ActivationCode()),
  );
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),

      home:  new Scaffold(
        backgroundColor: anCyan,

        body: ListView(
          children: <Widget>[

            // image Logo==========================================
            new Container(
              padding: EdgeInsets.only(top: 150 , left: 50 , right: 50),
              child: Image.asset("asset/Logo1.png",height: 85,),
            ),

            // text phone Number ======================================
            SizedBox(height: 30,),
            Align( alignment: Alignment.center,
                child: new Text("ادخل رقم الهاتف للمتابعة " , style: TextStyle(fontSize: 20 , color: anwhite),)
            ),

            // text TextField =========================================
            SizedBox(height: 30,),
            Directionality(textDirection: TextDirection.rtl,
                child: my_TextFieldMaterial(
                    Radius: 1 ,horizontal: 50 , elevation: 0,textInputType: TextInputType.phone ,
                    prefixIcon: Icons.phone_android ,HintText: "رقم الهاتف")
            ),


            // text TextField =========================================
            SizedBox(height: 30,),
            my_Button(
              onBtnclicked: (){_FunctionButtonNext();} ,
              horizontal: 120 , radiusButton: 25 , colorButton: anwhite , colorText: Colors.grey,
              textButton: "متابعة" , fontSize: 20
            )

          ],
        ),


      ),
    );
  }
}