import 'package:demo/Components/getlocation.dart';
import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:flutter/material.dart';
import 'package:avatar_glow/avatar_glow.dart';


class PageWhereGet extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<PageWhereGet> with SingleTickerProviderStateMixin {
  Animation animation , animationLogo ,animationCon;
  AnimationController animationController;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    animationController = AnimationController(vsync: this , duration: Duration(seconds: 2));

    animationLogo = Tween(begin: 1.0, end: -0.0008).animate(CurvedAnimation(
        curve: Interval(0.0, 1.0, curve: Curves.fastOutSlowIn),
        parent: animationController));

    animationController.forward();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double devHeight = MediaQuery.of(context).size.height;

    return AnimatedBuilder(
        animation: animationController,
        builder: (BuildContext context  ,Widget child){
          return MaterialApp(
            theme: ThemeData(fontFamily: "Cairo"),
            debugShowCheckedModeBanner: false,
            home: Scaffold(


              // appBar: AppBar(
              //   elevation: 0,
              //   leading: IconButton(icon: Icon(Icons.arrow_back_ios), onPressed: (){}),
              //   backgroundColor: anCyan,
              // ),

              body: ListView(
                children: <Widget>[

                  new Stack(
                    overflow: Overflow.visible,
                    children: <Widget>[

                      //====== هنا الجزء الذي سوف يظهر في الــ Maps ================
                      new Container(
                        height: MediaQuery.of(context).size.height-280,
                       child: GetLcationFromMap(),
                        // decoration: BoxDecoration(
                        //     color: Colors.grey.shade100,
                        //     image: DecorationImage(
                        //         fit: BoxFit.cover,
                        //         image: NetworkImage("https://images.pexels.com/photos/1974856/pexels-photo-1974856.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"))
                        // ),
                      ),

                      Positioned(
                          bottom: -90, left: 1, right: 1,
                          child: AvatarGlow(
                            glowColor: Colors.blue,
                            endRadius: 90.0,
                            duration: Duration(milliseconds: 2000),
                            repeatPauseDuration: Duration(milliseconds: 100),
                            child: Material(
                              elevation: 2,
                              shape: CircleBorder(),
                              child: CircleAvatar(
                                backgroundColor:anCyan ,
                                radius: 55.0,
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(55),
                                  ),
                                  child: Icon(Icons.location_on , size: 80, color: anwhite),
                                ),
                              ),
                            ),
                          )
                      ),


                    ],
                  ),
                  SizedBox(height: 80),


                  Directionality(textDirection: TextDirection.rtl,
                      child: my_TextFieldMaterial(Radius: 10 ,
                       elevation: 0,
                      //  fillcolor: prefix0.Color(0xffeeeeee),
                        horizontal: 20 ,
                      prefixIcon: Icons.search , HintText: "حدد مكان الوصول")),

                  SizedBox(height: 20),

                  my_Button(horizontal: 100 , radiusButton: 20 , onBtnclicked: (){} , textButton: "تحديد " ,colorButton: anCyan)

                ],
              ),

            ),
          );}
    );
  }
}
