import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

import 'package:toast/toast.dart';


class ShapMap extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<ShapMap> {

  void _FunctionMessage(){
    Toast.show("مراسلة السائق", context);
  }

  void _FunctionProblem(){
    Toast.show("الابلاغ عن مشكلة", context);
  }

  void _FunctionSend(){
    Toast.show("جاهز الان ", context);
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:  new Scaffold(

        //==bottomNavigationBar========================================
      bottomNavigationBar: Container(
          height: 70, color: anCyan,
          child: InkWell(onTap: (){_FunctionProblem();},
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("الابلاغ عن مشكلة" ,style: TextStyle(color: anwhite),),
                Icon(Icons.do_not_disturb_off ,color: anwhite,),
              ],
            ),
          ),
        ),


        //==AppBar========================================
        appBar: AppBar(
          elevation: 0,
          title: Text(" مشاكة  "),
        ),


        //==body========================================
        body: ListView(
          children: <Widget>[

            //------------الوقت  --------
            SizedBox(height: 20),
            Row(
              children: <Widget>[
                _buildExpanded(time: "00" , day: "يوم"),
                _buildExpanded(time: "01" , day: "ساعة"),
                _buildExpanded(time: "15" , day: "دقائق"),
            ],),

            //------------زر جاهز الان  --------
            SizedBox(height: 30),
            my_Button(
                onBtnclicked: (){_FunctionSend();}, horizontal: 100 , textButton: " جاهز الان " ,
                radiusButton: 5  , colorButton: anCyan
            ),
            SizedBox(height: 40),

            //------------الخريطة  --------
            new Container(
              margin: EdgeInsets.all(10),
              height: 250, color: Colors.grey[200],
            ),

            //------------زر مراسلة السائق  --------
            SizedBox(height: 1),
            my_ButtonIcon(
                onPressed: (){_FunctionMessage();}, horizontal: 100 , textButton: "مراسلة السائق  " ,
                radiusButton: 5  , colorButton: anCyan , icon: Icons.message
            ),





          ],
        ),







      ),
    );
  }

  Expanded _buildExpanded({
    String time = '00',
    String day = 'يوم',
   }){
    return Expanded(
                  child: Container(
                    margin: EdgeInsets.all(5),
                    height: 120 , color: Colors.grey[200],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                      new Text(time ,style: TextStyle(fontSize: 30 , fontWeight: FontWeight.bold , color: Colors.grey[600]),),
                      new Text(day ,style: TextStyle(fontSize: 18 , color: Colors.grey), ),
                    ],),
                  ));
  }


}