import 'package:demo/Components/NavigationBar.dart';
import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:demo/Tools/print.dart';
import 'package:demo/pages/home/home_search.dart';
import 'package:demo/pages/map/arrival%20_loaction.dart';
import 'package:demo/pages/map/leaving_location.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:toast/toast.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PageShareCar extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<PageShareCar> {

  void _FunactionSearch(){
    Toast.show("Start Search", context);
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => HomeSearch()),
  );
  }



  TextEditingController _controllerFrom = new TextEditingController();
  TextEditingController _controllerTo = new TextEditingController();
  TextEditingController _controllerDate = new TextEditingController();
  TextEditingController _controllerPeople = new TextEditingController();
  TextEditingController _controllerCar = new TextEditingController();

_FunctionLocation()
{
  printGreen("data");
   Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => LeavingLocation()),
  );
}
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:   ListView(
          children: <Widget>[
          new Directionality(textDirection: TextDirection.rtl, child: Column(children: <Widget>[
              //  InkWell(
              //    onTap: (){_FunctionLocation();},
            //  child:
              // my_TextFieldMaterial(
              //  readonly:true
              //  ,horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerFrom,
              //     prefixIcon: Icons.location_on , HintText: " مكان المغادرة ",textInputType: TextInputType.text),
              //  ),
            Row(
              children: <Widget>[
                 SizedBox(width: 15,),
                 new Icon( Icons.location_on,color:Colors.grey,size: 25,) ,
                 my_ButtonLoction(
                   colorButton:Colors.transparent,colorText: Colors.grey,
                textButton: " مكان المغادرة(من الخريطة) " , onBtnclicked: (){_FunctionLocation();} , fontSize: 16)
                     ,
                  
              ],
            ),
                   
            
              new Divider(),
  Row(
              children: <Widget>[
                 SizedBox(width: 15,),
                 new Icon( Icons.location_on,color:Colors.grey,size: 25,) ,
                 my_ButtonLoction(
                   colorButton:Colors.transparent,colorText: Colors.grey,
                textButton: "مكان الوصول(من الخريطة)" , onBtnclicked: (){_FunctionLocation();} , fontSize: 16)
                     ,
                  
              ],
            ),
              // my_TextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerTo,
              //     prefixIcon: Icons.location_on , HintText: " مكان الوصول ",textInputType: TextInputType.text),
              new Divider(),


              my_TextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerDate,
                  prefixIcon: FontAwesomeIcons.calendarAlt , HintText: " تاريخ رحلة المغادرة ",textInputType: TextInputType.text),
              new Divider(),


              my_TextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerPeople,
                  prefixIcon: FontAwesomeIcons.users , HintText: " عدد الاشخاص ",textInputType: TextInputType.text),
              new Divider(),


              my_TextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerCar,
                  prefixIcon: Icons.directions_car , HintText: " نوع السيارة ",textInputType: TextInputType.text),
              new Divider(),

              SizedBox(height: 50,),
              my_Button(horizontal: 20 , textButton: "إبدأ البحث " , onBtnclicked: (){_FunactionSearch();} ,colorButton: anCyan , fontSize: 18)

            ],)),
          ],
        ),



      
    );
  }
}