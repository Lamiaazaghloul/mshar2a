import 'package:demo/Tools/StyleApp.dart';
// import 'package:demo/pages/home/PageCarForRent.dart';
import 'package:flutter/material.dart';

import './PageShareCar.dart';
import './PageAirportCar.dart';



class PageTabBarView extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<PageTabBarView> {


  final List<Tab> tabs = <Tab>[
    Tab(text: "مشاركة", icon: Icon(Icons.directions_car,color: Colors.grey,),),
    Tab(text: "طلب سيارة للمطار", icon: Icon(Icons.location_on ,color: Colors.grey,),),
    Tab(text: "تأجير سيارة", icon: Icon(Icons.calendar_today ,color: Colors.grey,),),

  ];



  final List<Widget> WidgetTab = <Widget>[
    new PageShareCar(),
    new PageAirportCar(),
    new PageAirportCar(),
    
    // new PageCarForRent()
  ];


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:Directionality(
                textDirection: TextDirection.rtl,
                child: DefaultTabController(
        length:tabs.length,
        child: Scaffold(

          appBar: AppBar(
            title: Image.asset("asset/Logo1.png" ,height: 40,),
            leading: IconButton(icon: Icon(Icons.arrow_back_ios , size: 30,), onPressed: (){ Navigator.pop(context);}),
            elevation: 0,
            backgroundColor: anCyan,
            centerTitle: true,

            actions: <Widget>[
              IconButton(icon: Icon(Icons.history , size: 30,), onPressed: (){})
              // new Padding(padding: EdgeInsets.symmetric(horizontal: 20) ,
              // child: CircleAvatar(backgroundColor: anwhite, maxRadius: 15,
              // child: Icon(Icons.arrow_forward_ios, size: 20,),),)
            ],

            bottom: PreferredSize(
              child: Container(color: anwhite,
              child: TabBar(
              isScrollable: false,
              tabs: tabs,
              unselectedLabelColor: Colors.grey,
              labelColor: Colors.black,
              indicatorColor: Colors.amber,
            ),), preferredSize: Size.fromHeight(80))
          ),


          body:  TabBarView(
              children: WidgetTab
          ),

        ),
      ),
    ));
  }
}
