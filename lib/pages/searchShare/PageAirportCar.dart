import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:toast/toast.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PageAirportCar extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<PageAirportCar> {

  void _FunactionSearch(){
    Toast.show("Start Search", context);
  }



  TextEditingController _controllerFrom = new TextEditingController();
  TextEditingController _controllerTo = new TextEditingController();
  TextEditingController _controllerDate = new TextEditingController();
  TextEditingController _controllerPeople = new TextEditingController();
  TextEditingController _controllerCar = new TextEditingController();


  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:  


         ListView(
          children: <Widget>[
          new Directionality(textDirection: TextDirection.rtl, child: Column(children: <Widget>[

                  
              my_TextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerFrom,
                  prefixIcon: Icons.airplanemode_active , HintText: " من مطار ",textInputType: TextInputType.text),
              new Divider(),

              my_TextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerTo,
                  prefixIcon: Icons.location_on , HintText: " مكان الوصول ",textInputType: TextInputType.text),
              new Divider(),


              my_TextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerDate,
                  prefixIcon: FontAwesomeIcons.calendarAlt ,
                   HintText: " تاريخ رحلة المغادرة ",textInputType: TextInputType.text),
              new Divider(),


              my_TextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerPeople,
                  prefixIcon: FontAwesomeIcons.users , HintText: " عدد الاشخاص ",textInputType: TextInputType.text),
              new Divider(),


              my_TextFieldMaterial(horizontal: 0 , Radius: 4 , elevation: 0,controllers: _controllerCar,
                  prefixIcon: Icons.directions_car , HintText: " نوع السيارة ",textInputType: TextInputType.text),
              new Divider(),

              SizedBox(height: 50,),
              my_Button(horizontal: 20 , textButton: "إبدأ البحث " , onBtnclicked: (){_FunactionSearch();} ,colorButton: anCyan , fontSize: 18)

            ],)),
          ],
        ),



      
    );
  }
}