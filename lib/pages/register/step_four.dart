import 'dart:io';

import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:demo/pages/Activation_acount/phone_number.dart';
import 'package:demo/pages/login/login.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:toast/toast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class RegisterFour extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<RegisterFour> {

  File _LicensePhoto;       // صورة الرخصة
  File _IDPhoto;               // صورة الهوية
  File _CarBack;              // صور السيارة من الخلف
  File _CarFront;              // صور السيارة من الامام


  //--------- صورة الرخصة من الكاميرا-------------
  Future getLicensePhoto_Camera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    Navigator.pop(context,true);
    setState(() {
      _LicensePhoto = image;
    });
  }

  //--------- صورة الرخصة من المعرض -------------
  Future getLicensePhoto_gallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.pop(context,true);
    setState(() {
      _LicensePhoto = image;
    });
  }

  //--------- صورة الهوية من الكاميرا -------------
  Future getIDPhoto_Camera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    Navigator.pop(context,true);
    setState(() {
      _IDPhoto = image;
    });
  }

  //--------- صورة الهوية من المعرض -------------
  Future getIDPhoto_gallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.pop(context,true);
    setState(() {
      _IDPhoto = image;
    });
  }

  //--------- صور السيارة من الخلف من  الكاميرا -------------
  Future getCarBack_Camera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    Navigator.pop(context,true);
    setState(() {
      _CarBack = image;
    });
  }

  //--------- صور السيارة من الخلف من  المعرض -------------
  Future getICarBack_gallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.pop(context,true);
    setState(() {
      _CarBack = image;
    });
  }

  //--------- صور السيارة من الامام  من  الكاميرا -------------
  Future getCarFront_Camera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    Navigator.pop(context,true);
    setState(() {
      _CarFront = image;
    });
  }

  //--------- صور السيارة من الامام من  المعرض -------------
  Future getCarFront_gallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    Navigator.pop(context,true);
    setState(() {
      _CarFront = image;
    });
  }



  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";

  void _FunctionButtonNext(){
    // Toast.show("تسجيل", context);
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Login()),
  );  
  }



  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:  new Scaffold(
        backgroundColor: anCyan,


        body: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[

            // Header top=========================================
            new Stack(
              overflow: Overflow.visible,
              children: <Widget>[
                new Container(
                    height: 210, width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: anwhite,
                        image: DecorationImage(
                            colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.1), BlendMode.dstATop),
                            fit: BoxFit.fill,
                            image: NetworkImage(imageURL))
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(60.0),
                      child: Align(alignment: Alignment.center, child: Image.asset("asset/Logo2.png",height: 85,),),
                    )
                )
              ],
            ),
            SizedBox(height: 30),

            new Container(
              child: Row(
                children: <Widget>[

                  //===========================================صـــورة  الرخصــة==
                  Expanded(
                      child: GestureDetector(onTap: (){_DialogLicensePhoto(context);},
                        child: Container(
                          decoration: BoxDecoration( color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10)
                          ),
                          margin: EdgeInsets.all(5),
                          height: 130,
                          child: Center(child: _LicensePhoto == null ? _imageCar("صـــورة  الرخصــة") : Image.file(_LicensePhoto,fit: BoxFit.cover,)),
                        ),
                      )),

                  //===========================================صـــورة  الهــويــة==
                  Expanded(
                      child: GestureDetector(onTap: (){_DialogIDPhoto(context);},
                        child: Container(
                          decoration: BoxDecoration( color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10)
                          ),
                          margin: EdgeInsets.all(5),
                          height: 130,
                          child: Center(child: _IDPhoto == null ? _imageCar("صـــورة  الهــويــة") : Image.file(_IDPhoto,fit: BoxFit.cover,)),
                        ),
                      )),

                ],
              ),
            ),


            new Container(
              child: Row(
                children: <Widget>[

                  //===========================================صورة السيارة من الخلف==
                  Expanded(
                      child: GestureDetector(onTap: (){_DialogCarBack(context);},
                        child: Container(
                          decoration: BoxDecoration( color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10)
                          ),
                          margin: EdgeInsets.all(5),
                          height: 130,
                          child: Center(child: _CarBack == null ? _imageCar("صورة السيارة من الخلف") : Image.file(_CarBack,fit: BoxFit.cover,)),
                        ),
                      )),


                  //===========================================صورة السيارة من الامام==
                  Expanded(
                      child: GestureDetector(onTap: (){_DialogCarFront(context);},
                        child: Container(
                          decoration: BoxDecoration( color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10)
                          ),
                          margin: EdgeInsets.all(5),
                          height: 130,
                          child: Center(child: _CarFront == null ? _imageCar("صورة السيارة من الامام") : Image.file(_CarFront,fit: BoxFit.cover,)),
                        ),
                      )),

                ],
              ),
            ),


            // Button next =========================================
            SizedBox(height: 20,),
            my_Button(
                onBtnclicked: (){_FunctionButtonNext();} ,
                horizontal: 100 , radiusButton: 15 , colorButton: anwhite , colorText: Colors.grey,
                textButton: "تسجيل" , fontSize: 20
            ),
            SizedBox(height: 10),

          ],
        ),



      ),
    );
  }

  Widget _imageCar(String name) {
    return new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Text(name ,style: TextStyle(color: Colors.grey),),
        new Icon(Icons.image ,size: 70, color: Colors.grey[300],)
      ],);
  }

  //======= _Dialog صـــورة  الرخصــة===========================
  void _DialogLicensePhoto(BuildContext context) async {
    switch (await showDialog(context: context,
        builder: (BuildContext context)
        {
          return new SimpleDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
//                  title: const Text('هل تريد حذف هذا الطالب', style: TextStyle(fontFamily: "Cairo"),),
            children: <Widget>[

              new Icon(Icons.image , size: 100, color: anCyan),
              Center(child: new Text("قم باختيار صورة  الرخصة",style: TextStyle(fontFamily: "Cairo" , fontSize: 17),)),
              SizedBox(height: 20,),

              new Row( mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  //هنا اذا تم الضغط علي نعم سوف يتم استدعاء دالة حذف العنصر
                  new Container(
                    alignment: Alignment.center, height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: Colors.amber[400], borderRadius: BorderRadius.only(topLeft: Radius.circular(10) ,bottomLeft: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getLicensePhoto_gallery();},
                        child: Icon(FontAwesomeIcons.images , size: 35, color: Colors.white,)
                    ) ,
                  ),

                  //هنا اذا تم الضغط علي زر لا سوف يتم العود بدون فعل اي شي
                  new Container(alignment: Alignment.center,
                    height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: anCyan, borderRadius: BorderRadius.only(topRight: Radius.circular(10) ,bottomRight: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getLicensePhoto_Camera();},
                      child: Icon(FontAwesomeIcons.cameraRetro , size: 35, color: Colors.white,)
                    ),
                  ),

                ],
              ),

            ],
          );
        }
    ))
    {
      default:
    }
  }

  //======= _Dialog صـــورة  الهــويــة  ===========================
  void _DialogIDPhoto(BuildContext context) async {
    switch (await showDialog(context: context,
        builder: (BuildContext context)
        {
          return new SimpleDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
//                  title: const Text('هل تريد حذف هذا الطالب', style: TextStyle(fontFamily: "Cairo"),),
            children: <Widget>[

              new Icon(Icons.image , size: 100, color: anCyan),
              Center(child: new Text("قم باختيار الصورة الهوية",style: TextStyle(fontFamily: "Cairo" , fontSize: 17),)),
              SizedBox(height: 20,),

              new Row( mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  //هنا اذا تم الضغط علي نعم سوف يتم استدعاء دالة حذف العنصر
                  new Container(
                    alignment: Alignment.center, height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: Colors.amber[400], borderRadius: BorderRadius.only(topLeft: Radius.circular(10) ,bottomLeft: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getIDPhoto_gallery();},
                        child: Icon(FontAwesomeIcons.images , size: 35, color: Colors.white,)
                    ) ,
                  ),

                  //هنا اذا تم الضغط علي زر لا سوف يتم العود بدون فعل اي شي
                  new Container(alignment: Alignment.center,
                    height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: anCyan, borderRadius: BorderRadius.only(topRight: Radius.circular(10) ,bottomRight: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getIDPhoto_Camera();},
                      child: Icon(FontAwesomeIcons.cameraRetro , size: 35, color: Colors.white,)
                    ),
                  ),

                ],
              ),

            ],
          );
        }
    ))
    {
      default:
    }
  }

  //======= _Dialog  صور السيارة من الخلف===========================
  void _DialogCarBack(BuildContext context) async {
    switch (await showDialog(context: context,
        builder: (BuildContext context)
        {
          return new SimpleDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
//                  title: const Text('هل تريد حذف هذا الطالب', style: TextStyle(fontFamily: "Cairo"),),
            children: <Widget>[

              new Icon(Icons.image , size: 100, color: anCyan),
              Center(child: new Text("قم باختيار صورة السيارة من الخلف",style: TextStyle(fontFamily: "Cairo" , fontSize: 17),)),
              SizedBox(height: 20,),

              new Row( mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  //هنا اذا تم الضغط علي نعم سوف يتم استدعاء دالة حذف العنصر
                  new Container(
                    alignment: Alignment.center, height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: Colors.amber[400], borderRadius: BorderRadius.only(topLeft: Radius.circular(10) ,bottomLeft: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getICarBack_gallery();},
                        child: Icon(FontAwesomeIcons.images , size: 35, color: Colors.white,)
                    ) ,
                  ),

                  //هنا اذا تم الضغط علي زر لا سوف يتم العود بدون فعل اي شي
                  new Container(alignment: Alignment.center,
                    height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: anCyan, borderRadius: BorderRadius.only(topRight: Radius.circular(10) ,bottomRight: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getCarBack_Camera();},
                      child: Icon(FontAwesomeIcons.cameraRetro , size: 35, color: Colors.white,)
                    ),
                  ),

                ],
              ),

            ],
          );
        }
    ))
    {
      default:
    }
  }

  //======= _simple صورة السيارة من الامام  ===========================
  void _DialogCarFront(BuildContext context) async {
    switch (await showDialog(context: context,
        builder: (BuildContext context)
        {
          return new SimpleDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            children: <Widget>[

              new Icon(Icons.image , size: 100, color: anCyan),
              Center(child: new Text("قم باختيار صورة السيارة من الامام",style: TextStyle(fontFamily: "Cairo" , fontSize: 17),)),
              SizedBox(height: 20,),

              new Row( mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  //هنا اذا تم الضغط علي نعم سوف يتم استدعاء دالة حذف العنصر
                  new Container(
                    alignment: Alignment.center, height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: Colors.amber[400], borderRadius: BorderRadius.only(topLeft: Radius.circular(10) ,bottomLeft: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getCarFront_gallery();},
                        child: Icon(FontAwesomeIcons.images , size: 35, color: Colors.white,)
                    ) ,
                  ),

                  //هنا اذا تم الضغط علي زر لا سوف يتم العود بدون فعل اي شي
                  new Container(alignment: Alignment.center,
                    height: 60 , width: 120,
                    decoration: BoxDecoration(
                      color: anCyan, borderRadius: BorderRadius.only(topRight: Radius.circular(10) ,bottomRight: Radius.circular(10)),
                    ),
                    child:  new SimpleDialogOption(
                        onPressed: (){ getCarFront_Camera();},
                      child: Icon(FontAwesomeIcons.cameraRetro , size: 35, color: Colors.white,)
                    ),
                  ),

                ],
              ),

            ],
          );
        }
    ))
    {
      default:
    }
  }





}