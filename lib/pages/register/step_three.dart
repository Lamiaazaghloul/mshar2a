
import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:demo/pages/register/step_four.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';


class RegisterThree extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<RegisterThree> {

  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";

  TextEditingController _CarNumber  = new TextEditingController();
  TextEditingController _EndDate  = new TextEditingController();
  TextEditingController _LicensePlateNumber  = new TextEditingController();
  TextEditingController _CarType = new TextEditingController();
  TextEditingController _CarCategory = new TextEditingController();

  void _FunctionButtonNext(){
   Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => RegisterFour()),
  );  // Toast.show("التالي", context);

  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:  new Scaffold(
        backgroundColor: anCyan,


        body: ListView(
          children: <Widget>[

            // Header top=========================================
            new Stack(
              overflow: Overflow.visible,
              children: <Widget>[

                new Container(
                    height: 200, width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: anwhite,
                        image: DecorationImage(
                            colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.1), BlendMode.dstATop),
                            fit: BoxFit.cover,
                            image: NetworkImage(imageURL))
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(60.0),
                      child: Align(alignment: Alignment.center, child: Image.asset("asset/Logo2.png",),),
                    )
                ),
              ],
            ),
            SizedBox(height: 20),

            //  TextField رقــــــم العـــــربة   =========================================
            Directionality(textDirection: TextDirection.rtl,
                child: my_TextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.number , vertical: 3,
                    prefixIcon: Icons.phone_android ,HintText: "رقــم العـــربـة" , controllers: _CarNumber)
            ),


            //  TextField  تاريخ إنتهاء الرخصة =========================================
            Directionality(textDirection: TextDirection.rtl,
                child: my_TextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.datetime , vertical: 3,
                    prefixIcon: Icons.account_circle ,HintText: "تاريخ إنتهاء الرخصة" ,  controllers: _EndDate)
            ),


            //  TextField رقم لوحة السيارة =========================================
            Directionality(textDirection: TextDirection.rtl,
                child: my_TextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.number , vertical: 3,
                    prefixIcon: Icons.email ,HintText: "رقم لوحة السيارة " , controllers: _LicensePlateNumber)
            ),


            //  TextField نوع السيارة  =========================================
            Directionality(textDirection: TextDirection.rtl,
                child: my_TextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.text , vertical: 3,
                    prefixIcon: Icons.email ,HintText: "نوع السيارة  " , controllers: _CarType)
            ),


            //  TextField  فئة السيارة =========================================
            Directionality(textDirection: TextDirection.rtl,
                child: my_TextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.text , vertical: 3,
                    prefixIcon: Icons.email ,HintText: "فئة السيارة " , controllers: _CarCategory)
            ),


            // Button next =========================================
            SizedBox(height: 30,),
            my_Button(
                onBtnclicked: (){_FunctionButtonNext();} ,
                horizontal: 120 , radiusButton: 25 , colorButton: anwhite , colorText: Colors.grey,
                textButton: "التالي" , fontSize: 20
            )

          ],
        ),



      ),
    );
  }
}