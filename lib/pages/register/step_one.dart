
import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:demo/pages/register/step_two.dart';
import 'package:flutter/material.dart';


class RegisterOne extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<RegisterOne> {


  //  ============== ============================اكشن التفعيل
  void _FunctionButtonActive(){
    print("متابعة ");
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => RegisterTwo()),
  );
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),

      home:  new Scaffold(
        backgroundColor: anCyan,

        body: ListView(
          children: <Widget>[

            // image Logo==========================================
            new Container(
              padding: EdgeInsets.only(top: 150 , left: 50 , right: 50),
              child: Image.asset("asset/Logo1.png",height: 85,),
            ),



            // text TextField =========================================
            SizedBox(height: 45,),
            Directionality(textDirection: TextDirection.rtl,
                child: my_TextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.text ,
                    prefixIcon: Icons.account_circle ,HintText: "الاسم الاول")
            ),


            // text TextField =========================================
            SizedBox(height: 15,),
            Directionality(textDirection: TextDirection.rtl,
                child: my_TextFieldMaterial(
                    Radius: 10 ,horizontal: 30 , elevation: 0,textInputType: TextInputType.number ,
                    prefixIcon: Icons.account_balance_wallet ,HintText: "رقم الهوية")
            ),


            // text TextField =========================================
            SizedBox(height: 30,),
            my_Button(
                onBtnclicked: (){_FunctionButtonActive();} ,
                horizontal: 120 , radiusButton: 25 , colorButton: anwhite , colorText: Colors.grey,
                textButton: "التالي" , fontSize: 20
            )

          ],
        ),


      ),
    );
  }
}