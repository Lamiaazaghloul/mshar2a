import 'package:demo/Components/Connect.dart';
import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:flutter/material.dart';


class DriverDetails extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<DriverDetails> {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:  new Scaffold(

        //==AppBar==================================
        appBar: AppBar(
          title: Text(" تفاصيل المشارك  "),
          elevation: 0,
          backgroundColor: anCyan,
          centerTitle: true,
          actions: <Widget>[
            InkWell(onTap: (){},
              child: Padding(
                padding: const EdgeInsets.all(12),
                child: new CircleAvatar(backgroundColor: anwhite,child: Icon(Icons.arrow_forward_ios , size: 16,),),
              ),
            )
          ],
        ),


        //==Body==================================
        body: ListView(
          children: <Widget>[

            my_buildContaine(),

            //------------------------------------------------- تقيم السائق---
            new Container(
              color: Colors.white,
              height: 70,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                _buildColumn(name: "تبلغات" ,value: "0"),
                VerticalDivider(),
                _buildColumn(name: "تقيم" ,value: "456"),
                VerticalDivider(),
                _buildColumn(name: "متابع" ,value: "300"),
              ],
            ),),


            SizedBox(height: 10),
            //-------------------------------------------------- تقيم السائق---
            new Container(
              color: Colors.white,
              height: 70,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(" كيا سيراتو - 2017 "  , style: TextStyle(color: Colors.grey[500]),),

                VerticalDivider(),
                new Row(children: <Widget>[
                  Icon(Icons.directions_car ,size: 40 ,color: Colors.grey[500],),
                  Text("نوع السيارة" , style: TextStyle(color: Colors.grey[500]),),
                ],)
              ],
            ),),


            //-------------------------------------- الخدمات المشترك فيها السائق---
            SizedBox(height: 10),
            new Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              color: anwhite,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: new Text(" مشارك في  "),
                  ),
                  new Divider(height: 40,),

                  new Row(
                    children: <Widget>[
                      _buildExpanded(name: "تاجير يومي " , image: "asset/but4.png" , onTap: (){}),
                      _buildExpanded(name: "تاجير ساعة" , image: "asset/but3.png" , onTap: (){}),
                      _buildExpanded(name: "مطار" , image: "asset/but1.png" , onTap: (){}),
                      _buildExpanded(name: "مشاركة " , image: "asset/but2.png" , onTap: (){}),
                  ],)
                  
                ],
              ),
            ),



            //-------------------------------------------- المستندات والاثباتات---
            SizedBox(height: 10),
            new Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              color: anwhite,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  new Text(" المستندات والاثباتات  "),
                  new Divider(height: 20,),

                  new Row(
                    children: <Widget>[
                      _fileMan(),
                      _fileMan(),
                      _fileMan(),
                  ],)

                ],
              ),
            ),



            //-------------------------------------------- رقم الهوية و رقم الهاتف---
            SizedBox(height: 10),
            new Container(
              padding: EdgeInsets.symmetric(vertical: 5),
              color:anwhite,
              child: IntrinsicHeight(
                child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                  _NumberCard(name: "تاريخ انتهاء الرخصة"  , number: "878979987987"),
                  VerticalDivider(),
                  _NumberCard(name: "رقم الهاتف"  , number: "878979987987"),
                  VerticalDivider(),
                  _NumberCard(name: "رقم الهوية"  , number: "878979987987"),
                ],),
              ),
            ),


            SizedBox(height: 10),
            Container(

              color: anwhite,
              padding: EdgeInsets.symmetric(vertical: 10 , horizontal: 20),
              child: new Row(children: <Widget>[
                my_Button(textButton: "ارسال التعليق"  , radiusButton: 5 ,
                    heightButton: 40 , onBtnclicked: (){} , colorButton: anCyan),

               SizedBox(width: 10),
               Flexible(child: my_TextField(hintText: "كتابة تعليق" ,textAlign: TextAlign.center , horizontal: 0 , Radius: 2)),

              ],),
            )







          ],
        ),



      ),
    );
  }

  Column _NumberCard({ String name = "name" , String number = "11" }) {
    return new Column(children: <Widget>[
                  Text(name ,style: TextStyle(color: Colors.grey[400] , fontSize: 13),),
                  Text(number ,style: TextStyle(color: Colors.grey[400], fontSize: 13),),
                ],);
  }

  Expanded _fileMan() {
    return new Expanded(
                        child: Container(
                          margin: EdgeInsets.all(2),
                          height: 100,
                          color: Colors.grey[200],)
                    );
  }

  Widget _buildColumn({
    String name ="",
    String value ="",
}){
    return new Column(
                children: <Widget>[
                  Stack(overflow: Overflow.visible,
                    children: <Widget>[
                    Text(value , style: TextStyle(fontSize: 25 , fontWeight: FontWeight.bold , color: Colors.grey),),
                    Positioned(bottom: -15,child: Text(name)),
                  ],)
                ],
              );
  }


  Expanded _buildExpanded({
    String name = "Name" ,
    String image = "Name" ,
    GestureTapCallback onTap,
  }) { return Expanded(
    child: Container(
      margin: EdgeInsets.symmetric(horizontal: 3),
      height: 80,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [ BoxShadow(blurRadius: 1.0 , color: Colors.black.withOpacity(0.1))],
      ),
      child: Column(
        children: <Widget>[
          new Stack(overflow: Overflow.visible,
            children: <Widget>[
            new Image.asset(image , height: 60,),
            Positioned(bottom:-10 ,right: 0 , left: 0,
                child: Align(alignment: Alignment.center,child: Text(name , style: TextStyle(fontSize: 12 , color: Colors.grey),))),
          ],)
        ],),
    ),
  );
  }


}