


import 'package:demo/Components/Connect.dart';
import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';



class TripDetails extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<TripDetails> {

  void _FunctionArrange(){
    Toast.show("Arrange", context);
  }

  String imageMan = 'https://cdn.pixabay.com/photo/2015/01/08/18/29/entrepreneur-593358__480.jpg';

  void _FunctionProblem(){
    Toast.show("الابلاغ عن مشكلة", context);
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:  new Scaffold(
        backgroundColor: Colors.white,

        //==bottomNavigationBar========================================
        bottomNavigationBar: Container(
          height: 50, color: anCyan,
          child: InkWell(onTap: (){_FunctionProblem();},
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("الابلاغ عن مشكلة" ,style: TextStyle(color: anwhite),),
                Icon(Icons.do_not_disturb_off ,color: anwhite,),
              ],
            ),
          ),
        ),



        //==AppBar========================================
        appBar: AppBar(
          elevation: 0,
          centerTitle: true,
          title: Text("تفاصيل الرحلة"),
          backgroundColor: anCyan,
          leading: IconButton(icon: Icon(Icons.arrow_back_ios), onPressed: (){}),
        ),


        //==body========================================
        body: ListView(
          children: <Widget>[

            //------------- صور السيارة---------
            new Container( height: 200,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage("asset/car_cover.png"))
            ),
            ),

            //------------- معلومات عن السائق ---------
            my_buildContainer44(imageUrl: imageMan,
           
            ),

            //------------- تفاصيل السائق ---------
            new Container(
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    new Text("تفاصيل عن السائق" ),
                    new Text("لكن كتعريف علمى او كما يعرفه موقع المطورين هو عباره عن سلوك او واجهة مستخدم يمكن تضمينه بداخل Activity  اى انه عباره عن شىء مثل الاكتيتفى لكن يمكن وضعه داخل أكتيتفى أخر  حيث فى الوضع العادى لا يمكن وضع Activity داخل Activity أخر او وضع اثنين من الـ Activities فى شاشة واحدة  أكثر تنظيما" ,
                      style: TextStyle(color: Colors.grey), textAlign: TextAlign.right,),

                    //------------- من جدة الي الرياض ---------
                    new Divider(height: 30,),
                    my_buildContainer55()

                  ],
                ),
              ),
            ),

            //------------زر مراسلة السائق  --------
            SizedBox(height: 20),
            my_ButtonIcon(
                onPressed: (){}, horizontal: 100 , textButton: "مراسلة السائق  " ,
                radiusButton: 5  , colorButton: anCyan , icon: Icons.message
            ),
            SizedBox(height: 40),
          ],
        ),


      ),
    );
  }
}