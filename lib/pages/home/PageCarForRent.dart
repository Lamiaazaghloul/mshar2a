import 'package:demo/Components/Connect.dart';
import 'package:demo/Tools/StyleApp.dart';
import 'package:flutter/material.dart';

import 'package:toast/toast.dart';


class PageCarForRent extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<PageCarForRent> {


  void _FunctionArrange(){
    Toast.show("Arrange", context);
  }

  void _FunctionFilter(){
    Toast.show("Filter", context);
  }

  void _FunctionButtonMenu(){
    Toast.show("ButtonMenu", context);
  }

  void _FunctionButtonback(){
     Navigator.pop(context);
    Toast.show("Buttonback", context);
  }

  void _FunctionSAR(){
    Toast.show("SAR", context);
  }

  TextEditingController _SearchController = new TextEditingController();

  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";
  String imageMan = "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500";


  bool value4 = false;
  void onChangedValue4( bool value){
    setState(() {
      value4 = value;
      if(value4 == true){
        Toast.show("ON", context);
      }else{
        Toast.show("OFF", context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:  new Scaffold(

        //===AppBar=============================================
        appBar: AppBar(
          elevation: 0,
          backgroundColor: anCyan,
          centerTitle: true,

          //----------TextField Search -----------------
          title:Text("تأجير سيارة"),

          //----------Button back next to search -----------------
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InkWell(onTap: (){_FunctionButtonback();},
                child: CircleAvatar(backgroundColor: anwhite, child: Icon(Icons.arrow_forward_ios ,size: 15,),),),
            ),
          ],

          //----------bottom -----------------
          bottom: PreferredSize(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 0 , vertical: 15),
                child: IntrinsicHeight(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){_FunctionFilter();},
                        child: new Row(
                          children: <Widget>[
                            Text("شركات التاجير "  ,style: TextStyle(color: anwhite),),
                            Icon(Icons.directions_car,color: anwhite , size: 15)
                          ],
                        ),
                      ),

                      VerticalDivider(),
                      GestureDetector(onTap: (){_FunctionFilter();},
                        child: new Row(
                          children: <Widget>[
                            Text("الفلتر"  ,style: TextStyle(color: anwhite),),
                            Icon(Icons.menu,color: anwhite , size: 15)
                          ],
                        ),
                      ),


                      VerticalDivider(),
                      GestureDetector(onTap: (){_FunctionFilter();},
                        child: new Row(
                          children: <Widget>[
                            Text("ترتيب حسب"  ,style: TextStyle(color: anwhite),),
                            Icon(Icons.menu,color: anwhite , size: 15)
                          ],
                        ),
                      ),

                      VerticalDivider(),
                      GestureDetector(onTap: (){_FunctionArrange();},
                        child: new Row(
                          children: <Widget>[
                            Text("المدينة" ,style: TextStyle(color: anwhite),),
                            Icon(Icons.location_on,color: anwhite , size: 15),
                          ],
                        ),
                      ),

                    ],
                  ),
                ),
              ), preferredSize: Size.fromHeight(50)),
        ),

        //===bottomNavigationBar=============================================
        bottomNavigationBar: Container(
          height:50,
          color: anCyan,
          child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[

              GestureDetector(onTap: (){_FunctionSAR();},
                child: new Row(children: <Widget>[
                  Text(" الاجمالي 30 ريال " ,style: TextStyle(color: anwhite , fontSize: 12),) ,
                ],),
              ),

              VerticalDivider(color: anwhite),
              Text(" سعر اليوم 15 ريال " ,style: TextStyle(color: anwhite , fontSize: 12),) ,
            VerticalDivider(color: anwhite),

            GestureDetector(onTap: (){_FunctionSAR();},
              child: new Row(children: <Widget>[
                Icon(Icons.arrow_drop_down ,color: anwhite),
                Text(" SAR " ,style: TextStyle(color: anwhite , fontSize: 12),) ,
              ],),
            ),


          ],)
        ),


        body: ListView(children: <Widget>[
          my_buildContainer5(onTap: (){} , imageUrl: imageMan),
          my_buildContainer5(onTap: (){} , imageUrl: imageMan),
          my_buildContainer5(onTap: (){} , imageUrl: imageMan),
          my_buildContainer5(onTap: (){} , imageUrl: imageMan),
          my_buildContainer5(onTap: (){} , imageUrl: imageMan),
          my_buildContainer5(onTap: (){} , imageUrl: imageMan),
          my_buildContainer5(onTap: (){} , imageUrl: imageMan),
          my_buildContainer5(onTap: (){} , imageUrl: imageMan),
        ],),



      ),
    );
  }
}