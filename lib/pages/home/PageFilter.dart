import 'package:demo/Components/Connect.dart';
import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:flutter/material.dart';

import 'package:toast/toast.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class PageFilter extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<PageFilter> {





void _FunctionArrange(context){
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc){
          return Container(
            child: new Wrap(
            children: <Widget>[

              //Nearby
new ListTile(
            leading: new Icon(Icons.music_note),
            title: new Text('الأقرب'),
            onTap: () => {}          
          ),
          //price
          new ListTile(
            leading: new Icon(Icons.videocam),
            title: new Text('الاقل سعر'),
            onTap: () => {},          
          ),

                        //car new
new ListTile(
            leading: new Icon(Icons.music_note),
            title: new Text('نوع السيارة الاحدث'),
            onTap: () => {}          
          ),
          //car old
          new ListTile(
            leading: new Icon(Icons.videocam),
            title: new Text('نوع السيارة الاقدم'),
            onTap: () => {},          
          ),

                        //car smal
new ListTile(
            leading: new Icon(Icons.music_note),
            title: new Text('حجم السيارة الاكبر'),
            onTap: () => {}          
          ),
          //car big
          new ListTile(
            leading: new Icon(Icons.videocam),
            title: new Text('حجم السارة الاصغر'),
            onTap: () => {},          
          ),

            ],
          ),
          );
      }
    );
}



  // void _FunctionArrange(){
  //   Toast.show("Arrange", context);
  // }

  void _FunctionFilter(){
    Toast.show("Filter", context);
  }

  void _FunctionButtonMenu(){
    Toast.show("ButtonMenu", context);
  }

  void _FunctionButtonback(){
    Toast.show("Buttonback", context);
  }

  void _FunctionSAR(){
    Toast.show("SAR", context);
  }

  TextEditingController _SearchController = new TextEditingController();

  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";
  String imageMan = "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500";


  bool value4 = false;
  void onChangedValue4( bool value){
    setState(() {
      value4 = value;
      if(value4 == true){
        Toast.show("ON", context);
      }else{
        Toast.show("OFF", context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:  new Scaffold(

        //===AppBar=============================================
        appBar: AppBar(
          elevation: 0,
          backgroundColor: anCyan,
          centerTitle: true,

          //----------TextField Search -----------------
          title:Directionality(textDirection: TextDirection.rtl,
          child: my_TextFieldMaterial(controllers: _SearchController,elevation: 0,HintText: "",
          prefixIcon: FontAwesomeIcons.mapMarkerAlt ,Radius: 10,prefixIconSize: 20)),

          //----------Button back next to search -----------------
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InkWell(onTap: (){_FunctionButtonback();},
              child: CircleAvatar(backgroundColor: anwhite, child: Icon(Icons.arrow_forward_ios ,size: 15,),),),
            ),
          ],

          //----------bottom -----------------
          bottom: PreferredSize(
          child: IntrinsicHeight(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                IconButton(icon: Icon(Icons.menu,color: anwhite), onPressed: (){_FunctionButtonMenu();}),
                VerticalDivider(),

                GestureDetector(onTap: (){_FunctionFilter();},
                  child: new Row(
                    children: <Widget>[
                      Text("الفلتر"  ,style: TextStyle(color: anwhite),),SizedBox(width: 10),
                      Icon(FontAwesomeIcons.sortAmountDownAlt,color: anwhite , size: 15)
                    ],
                  ),
                ),

                VerticalDivider(),
                GestureDetector(onTap: (){_FunctionArrange(context);},
                  child: new Row(
                    children: <Widget>[
                      Text(" ترتيب حسب" ,style: TextStyle(color: anwhite),), SizedBox(width: 10),
                      Icon(FontAwesomeIcons.alignRight,color: anwhite , size: 15,)
                    ],
                  ),
                ),

              ],
            ),
          ), preferredSize: Size.fromHeight(40)),
        ),

        //===bottomNavigationBar=============================================
        bottomNavigationBar: Container(
          height: 75,
          color: anCyan,
          child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
            new Row(children: <Widget>[
            new Switch(value: value4, onChanged: onChangedValue4 ,activeColor: Colors.amber,),
            Text(" احجز الان " ,style: TextStyle(color: anwhite , fontSize: 12),) ,

            ],),
            VerticalDivider(color: anwhite),

            Text(" شامل الرسوم " ,style: TextStyle(color: anwhite , fontSize: 12),) ,
            VerticalDivider(color: anwhite),

            GestureDetector(onTap: (){_FunctionSAR();},
              child: new Row(children: <Widget>[
                Icon(Icons.arrow_drop_down ,color: anwhite),
                Text(" SAR " ,style: TextStyle(color: anwhite , fontSize: 12),) ,
              ],),
            ),

            VerticalDivider(color: anwhite),
            Text(" شخص واحد " ,style: TextStyle(color: anwhite , fontSize: 12),) ,

          ],)
        ),


        body: ListView(children: <Widget>[
          my_buildContainer1(onTap: (){} , imageUrl: imageMan),
          my_buildContainer1(onTap: (){} , imageUrl: imageMan),
          my_buildContainer1(onTap: (){} , imageUrl: imageMan),
          my_buildContainer1(onTap: (){} , imageUrl: imageMan),
          my_buildContainer1(onTap: (){} , imageUrl: imageMan),
          my_buildContainer1(onTap: (){} , imageUrl: imageMan),
        ],),



      ),
    );
  }
}