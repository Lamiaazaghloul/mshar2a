import 'package:demo/Components/Connect.dart';
import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:demo/Tools/textField.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:toast/toast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeSearch extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<HomeSearch> {

double _result = 0.0;
int _radioValue;
void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;
  
      switch (_radioValue) {
        case 0:
          // _result = 
          break;
        case 1:
          // _result = ...
          break;
        case 2:
          // _result = ...
          break;
      }
    });
  }


  void _FunctionArrange(context){
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc){
          return Directionality(textDirection: TextDirection.rtl,
          child:Container(
            child: new Wrap(
            children: <Widget>[



new ListTile(
            leading: new Icon(Icons.list),
            title: new Text('ترتيب حسب'),
            onTap: () => {}          
          ),
          


           new Divider(),
              //Nearby
new ListTile(
            leading: new Icon(Icons.location_on),
            title: new Text('الأقرب'),
            onTap: () => {}          
          ),
          //price
          new ListTile(
            leading: new Icon(Icons.attach_money),
            title: new Text('الاقل سعر'),
            onTap: () => {},          
          ),

                        //car new
new ListTile(
            leading: new Icon(Icons.local_car_wash),
            title: new Text('نوع السيارة الاحدث'),
            onTap: () => {}          
          ),
          //car old
          new ListTile(
            leading: new Icon(Icons.directions_car),
            title: new Text('نوع السيارة الاقدم'),
            onTap: () => {},          
          ),

                        //car smal
new ListTile(
            leading: new Icon(Icons.add_circle),
            title: new Text('حجم السيارة الاكبر'),
            onTap: () => {}          
          ),
          //car big
          new ListTile(
            leading: new Icon(Icons.remove_circle),
            title: new Text('حجم السارة الاصغر'),
            onTap: () => {},          
          ),

            ],
          )),
          );
      }
    );
}

int _currVal = 1;
  String _currText = '';

  List<GroupModel> _group = [
    GroupModel(
      text: "Flutter.dev",
      index: 1,
    ),
    GroupModel(
      text: "Inducesmile.com",
      index: 2,
    ),
    GroupModel(
      text: "Google.com",
      index: 3,
    ),
    GroupModel(
      text: "Yahoo.com",
      index: 4,
    ),
  ];

 


  void _FunctionFilter(context){
    showModalBottomSheet(
      context: context,
      builder: (BuildContext bc){
          return Directionality(textDirection: TextDirection.rtl,
          child:Container(
            child: new Wrap(
            children: <Widget>[

new ListTile(
            leading: new Icon(FontAwesomeIcons.sortAmountDownAlt , size: 15),
            title: new Text('الفلتر'),
            onTap: () => {}          
          ),
          
           new Divider(),
              //Nearby
new ListTile(
            leading: new Icon(Icons.location_on),
            title: new Text('محطات'),
            onTap: () => {}          
          ),
          //price
          new ListTile(
            leading: new Icon(Icons.directions_car),
            title: Row(children: <Widget>[
              Text(
        "احجام السيارة",style:TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
      ), 
      new Radio(
        value: 0,
        groupValue: _radioValue,
        onChanged: _handleRadioValueChange,
      ),
       Text(
        "كبيره",style:TextStyle(fontSize: 10)
      ),
      new Radio(
        value: 1,
        groupValue: _radioValue,
        onChanged: _handleRadioValueChange,
      ),
       Text(
        "متوسطة",style:TextStyle(fontSize: 10)
      ),
      new Radio(
        value: 2,
        groupValue: _radioValue,
        onChanged: _handleRadioValueChange,
      ),
       Text(
        "صغيرة",style:TextStyle(fontSize: 10)
      ),
    
            ],),
            // new Text('أحجام السيارات'),
            onTap: () => {},          
          ),

                        //car new
new ListTile(
            leading: new Icon(Icons.attach_money),
            title: new Row(children: <Widget>[
new Text('السعر'),
SizedBox(width: 15,),
new Text('من'),
SizedBox(width: 15,),
new Text('20'),

SizedBox(width: 15,),
new Text('الي'),
SizedBox(width: 15,),
new Text('60'),
SizedBox(width: 15,),
new Text('ريال'),
            ],),
            onTap: () => {}          
          ),

          //car old
          new ListTile(
            leading: new Icon(Icons.merge_type),
            title: Row(children: <Widget>[
              Text(
        "النوع",style:TextStyle(fontSize: 12,fontWeight:FontWeight.bold),
      ), 
      new Radio(
        value: 0,
        groupValue: _radioValue,
        onChanged: _handleRadioValueChange,
      ),
       Text(
        "ذكر",style:TextStyle(fontSize: 10)
      ),
      new Radio(
        value: 1,
        groupValue: _radioValue,
        onChanged: _handleRadioValueChange,
      ),
       Text(
        "أنثي",style:TextStyle(fontSize: 10)
      ),
      
    
            ],),
            onTap: () => {},          
          ),

            ],
          )),
          );
      }
    );
}


  void _FunctionButtonMenu(){
    
    Toast.show("ButtonMenu", context);
  }

  void _FunctionButtonback(){
//    Toast.show("Buttonback", context);
    Navigator.pop(context);
  }

  void _FunctionSAR(){
    Toast.show("SAR", context);
  }

  TextEditingController _SearchController = new TextEditingController();

  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";
  String imageMan = "https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500";


  bool value4 = false;
  void onChangedValue4( bool value){
    setState(() {
      value4 = value;
      if(value4 == true){
        Toast.show("ON", context);
      }else{
        Toast.show("OFF", context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:  new Scaffold(
        //===AppBar=============================================
        appBar: AppBar(
          elevation: 0,
          backgroundColor: anCyan,
          centerTitle: true,

          //----------TextField Search -----------------
          title:Directionality(textDirection: TextDirection.rtl,
              child:Padding(
                  padding: const EdgeInsets.only(top: 10.0,right: 15),
                  child:my_TextFieldMaterial(
//                     padding_inslide: 5,
                      controllers: _SearchController,elevation: 0,HintText: "",
                  prefixIcon: FontAwesomeIcons.mapMarkerAlt ,Radius: 10,prefixIconSize: 20)
              )),

          //----------Button back next to search -----------------
          actions: <Widget>[
            Container(
              margin:const EdgeInsets.only( top:12,right: 5),
              padding: const EdgeInsets.all(0),
              child: InkWell(onTap: (){_FunctionButtonback();},
                child: CircleAvatar(backgroundColor: anwhite, child: Icon(Icons.arrow_forward_ios ,size: 15,),),),
            ),
          ],

          //----------bottom -----------------
          bottom: PreferredSize(
              child: IntrinsicHeight(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    IconButton(icon: Icon(Icons.menu,color: anwhite), onPressed: (){_FunctionButtonMenu();}),
                    VerticalDivider(),

                    GestureDetector(onTap: (){_FunctionFilter(context);},
                      child: new Row(
                        children: <Widget>[
                          Text("الفلتر"  ,style: TextStyle(color: anwhite),),SizedBox(width: 10),
                          Icon(FontAwesomeIcons.sortAmountDownAlt,color: anwhite , size: 15)
                        ],
                      ),
                    ),

                    VerticalDivider(),
                    GestureDetector(onTap: (){_FunctionArrange(context);},
                      child: new Row(
                        children: <Widget>[
                          Text(" ترتيب حسب" ,style: TextStyle(color: anwhite),), SizedBox(width: 10),
                          Icon(FontAwesomeIcons.alignRight,color: anwhite , size: 15,)
                        ],
                      ),
                    ),

                  ],
                ),
              ), preferredSize: Size.fromHeight(40)),
        ),
       
        //===bottomNavigationBar=============================================
        bottomNavigationBar: Container(
            height: 50,
            color: anCyan,
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                new Row(children: <Widget>[
                  new Switch(value: value4, onChanged: onChangedValue4 ,activeColor: Colors.green,),
                  Text(" احجز الان " ,style: TextStyle(color: anwhite , fontSize: 12),) ,

                ],),
                VerticalDivider(color: anwhite),

                Text(" شامل الرسوم " ,style: TextStyle(color: anwhite , fontSize: 12),) ,
                VerticalDivider(color: anwhite),

                GestureDetector(onTap: (){_FunctionSAR();},
                  child: new Row(children: <Widget>[
                    Icon(Icons.arrow_drop_down ,color: anwhite),
                    Text(" SAR " ,style: TextStyle(color: anwhite , fontSize: 12),) ,
                  ],),
                ),

                VerticalDivider(color: anwhite),
                Text(" شخص واحد " ,style: TextStyle(color: anwhite , fontSize: 12),) ,

              ],)
        ),


        body: ListView(children: <Widget>[
        SizedBox(height:10),
          my_buildContainer2(onTap: (){} , imageUrl: imageMan),
          my_buildContainer2(onTap: (){} , imageUrl: imageMan),
          my_buildContainer2(onTap: (){} , imageUrl: imageMan),
          my_buildContainer2(onTap: (){} , imageUrl: imageMan),
          my_buildContainer2(onTap: (){} , imageUrl: imageMan),
          my_buildContainer2(onTap: (){} , imageUrl: imageMan),
        ],),



      ),
    );
  }
}
class GroupModel {
  String text;
  int index;
  GroupModel({this.text, this.index});
}