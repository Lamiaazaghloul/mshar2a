import 'package:demo/Components/Connect.dart';
import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/print.dart';

import 'package:demo/pages/home/PageCarForRent.dart';
import 'package:demo/pages/home/car_for_hour.dart';
import 'package:demo/pages/home/home_search.dart';
import 'package:demo/pages/home/requestAirport.dart';
import 'package:demo/pages/searchShare/PageTabBarView.dart';
import 'package:demo/pages/trip/Page_DriverDetails.dart';
import 'package:demo/pages/trip/trip_details.dart';
import 'package:flutter/material.dart';

import 'package:toast/toast.dart';

import '../../allTranslations.dart';


class HomePage extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<HomePage> {

  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";
  String imageMan = "https://experience.sap.com/fiori-design-web/wp-content/uploads/sites/5/2017/02/Avatar-Sizes-Custom-1.png";

  @override
    void initState(){
        super.initState();
      
    }
  void _FunctionSearch(){
//    Toast.show("Search", context);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => HomeSearch()),
    );

  }
  void _FunctionDrawer(){ Toast.show("drawer", context); }
  void _FunctionShare(){ 
    Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => PageTabBarView()),
  );
    // Toast.show("Share", context);

   }
  void _FunctionAirport(){ 
    // Toast.show("Airport", context);
   Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => RequestAirport()),
  );
  
   }
  void _FunctionHour(){ 
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => PageCarForHour()),
  );
    
    // Toast.show("Hour", context);
     }
  void _FunctionDay(){ 
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => PageCarForRent()),
  );
  // Toast.show("Day", context);
   }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:  new Scaffold(



        body: Column(
          children: <Widget>[

            new Stack(
              alignment: Alignment.center,
              overflow: Overflow.visible,
              children: <Widget>[

                // ====================================المربع الذي يظهر تحت الوجو ==
                new Container(
                  height: 250.0,
                  decoration: BoxDecoration(
                    color: anCyan,
                    image: DecorationImage(
                    colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.1), BlendMode.dstATop),
                    fit: BoxFit.cover,image: NetworkImage(imageURL)),
                    borderRadius: BorderRadius.only( bottomRight: Radius.circular(30.0), bottomLeft: Radius.circular(30.0),),
                  ),
                  child: new Column(
                    children: <Widget>[

//                      =========================== Icons Search and Icons Drwar
                      Stack(overflow: Overflow.visible,
                      children: <Widget>[

                      new Container(padding: EdgeInsets.symmetric(horizontal: 10.0 , vertical: 40.0),
                     child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: <Widget>[
                       new IconButton(icon: Icon(Icons.format_align_left,size: 30, color: anwhite,), onPressed: (){_FunctionDrawer();}),
                       new IconButton(icon: Icon(Icons.search,size: 30, color: anwhite,), onPressed: (){_FunctionSearch();}),
                      ],
                     ),
                      ),

                      Positioned(
                        bottom: -50,right: 0,left: 0,
                          child: Align(alignment: Alignment.center,child: new Image.asset("asset/Logo1.png" , width: 250,))),
                      ],
                      ),
                    ],),
                ),

                //=====================================هنا كود المربع الذي تحت الصورة
                Positioned( right: 15,  left: 15,  bottom: -50.0,
                  child: new Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    decoration: BoxDecoration(
//                      color: Colors.grey[100],
                      borderRadius: BorderRadius.circular(10.0),
//                      boxShadow: [ BoxShadow(blurRadius: 20.0 , color: Colors.black.withOpacity(0.2))],
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        _buildExpanded(name: "تاجير يومي" , image: "asset/but4.png" , onTap: (){
                          // _FunctionDay();
                            Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => PageCarForHour()),
  );
                          }),
                        _buildExpanded(name: "تاجير ساعة" , image: "asset/but3.png" , onTap: (){
                          // _FunctionHour();
                            Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => PageCarForHour()),
  );
                          }),
                        _buildExpanded(name: "مطار" , image: "asset/but2.png" , onTap: (){
                          // _FunctionAirport();
                           Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => RequestAirport()),
  );
                          }),
                        _buildExpanded(name: 'مشاركة' , image: "asset/but1.png" , onTap: (){
                          // _FunctionShare();
                          Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => PageTabBarView()),
  );
                        }),
                      ],
                    ),
                  ),
                ),

              ],
            ),
            SizedBox(height: 50),

            Expanded(
              child: new SingleChildScrollView(
                child: Column(children: <Widget>[
                 my_buildContainer1(details:(){
                     
                     Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => TripDetails()),
    );
                  },
                    onTap: (){
                    //  Toast.show("تم الحجز", context);
                      Toast.show("تم الحجز", context);
Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => DriverDetails()),
    );
                    } , imageUrl: imageMan),
               my_buildContainer1(details:(){
                     
                     Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => TripDetails()),
    );
                  },
                    onTap: (){
Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => DriverDetails()),
    );
                      
                     Toast.show("تم الحجز", context);
                    } , imageUrl: imageMan),
                  my_buildContainer1(details:(){
                     
                     Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => TripDetails()),
    );
                  },
                    onTap: (){
                     Toast.show("تم الحجز", context);
                    } , imageUrl: imageMan),
                my_buildContainer1(details:(){
                     
                     Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => TripDetails()),
    );
                  },
                    onTap: (){
                     Toast.show("تم الحجز", context);
                    } , imageUrl: imageMan),
                  my_buildContainer1(details:(){
                     
                     Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => TripDetails()),
    );
                  },
                    onTap: (){
                     Toast.show("تم الحجز", context);
                    } , imageUrl: imageMan),
              ],),),
            )

          ],
        ),





      ),
    );
  }

  Expanded _buildExpanded({
    String name = "Name" ,
    String image = "Name" ,
    GestureTapCallback onTap,
    }) { return Expanded(

                        child:InkWell(onTap:onTap,
                         child:Container(
                          margin: EdgeInsets.symmetric(horizontal: 3),
                          height: 80,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                            boxShadow: [ BoxShadow(blurRadius: 5.0 , color: Colors.black.withOpacity(0.2))],
                          ),
                          child: Column(

                            children: <Widget>[
                              
                              new Stack(children: <Widget>[
                                new Image.asset(image , height: 65,),
                                Positioned(bottom:-5 ,right: 0 , left: 0,
                                    child: Align(alignment: Alignment.center,child: Text(name , style: TextStyle(fontSize: 12 , color: Colors.grey),))),
                              ],),
                            ],),
                        )),
                      );
  }

}


