
import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:demo/pages/forgotpassword/message_confirm.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:toast/toast.dart';

class ResetPassword extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<ResetPassword> {

  String imageURL = "https://cdn.pixabay.com/photo/2019/04/02/10/58/oldtimer-4097480__480.jpg";

  TextEditingController _EmailPassword  = new TextEditingController();

  void _ForgotPassword(){
    Toast.show("هل نسيت كلمة السر ", context);
  }

  void _ResetPassword(){
    Toast.show(" Reset Password ", context);
     Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Page_SendCodeActive()),
  );
  }


  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:  new Scaffold(
        backgroundColor: anCyan,


        body: ListView(
          children: <Widget>[

            // Header top=========================================
            new Stack(
              overflow: Overflow.visible,
              children: <Widget>[

                new Container(
                    height: 250, width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: anwhite,
                        image: DecorationImage(
                            colorFilter: new ColorFilter.mode(Colors.white.withOpacity(0.1), BlendMode.dstATop),
                            fit: BoxFit.cover,
                            image: NetworkImage(imageURL))
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(60.0),
                      child: Align(alignment: Alignment.center, child: Image.asset("asset/Logo2.png"),),
                    )
                ),

                Positioned(
                    bottom: -60, left: MediaQuery.of(context).size.width /2 -55,
                    child: my_IconInContainer(
                      color1: anCyan ,color2: Colors.white, heightWidth: 120,
                      widget: Icon(Icons.account_circle ,size: 110 ,color: anCyan,) ,
                    )),

              ],
            ),
            SizedBox(height: 80),

//            Padding(
//              padding: const EdgeInsets.all(20),
//              child: Align(alignment: Alignment.center,
//                  child: InkWell(onTap: (){_ForgotPassword();},
//                      child: new Text("قم بإدخال بريدك الإكتروني المسجل لدينا لكي تتمكن من تعين كلمة مرور جديدة" ,style: TextStyle(color: anwhite , fontSize: 20),textAlign: TextAlign.center,))
//              ),
//            ),

            //  TextField phone  =========================================
            Directionality(textDirection: TextDirection.rtl,
                child: my_TextFieldMaterial(
                    Radius: 5 ,horizontal: 20 , elevation: 0,textInputType: TextInputType.phone , vertical: 4,
                    prefixIcon: Icons.email ,HintText: "رمز التفعيل" , controllers: _EmailPassword)
            ),
            // Button next =========================================
            SizedBox(height: 5,),

            //  TextField phone  =========================================
            Directionality(textDirection: TextDirection.rtl,
                child: my_TextFieldMaterial(
                    Radius: 5 ,horizontal: 20 , elevation: 0,textInputType: TextInputType.phone , vertical: 4,
                    prefixIcon: Icons.email ,HintText: "كلمة المرور الجديدة" , controllers: _EmailPassword)
            ),
            // Button next =========================================
            SizedBox(height:5,),


            //  TextField phone  =========================================
            Directionality(textDirection: TextDirection.rtl,
                child: my_TextFieldMaterial(
                    Radius: 5 ,horizontal: 20 , elevation: 0,textInputType: TextInputType.phone , vertical: 4,
                    prefixIcon: Icons.email ,HintText: "تأكيد كلمة المرور" , controllers: _EmailPassword)
            ),
            // Button next =========================================
            SizedBox(height: 20,),
            my_Button(
                onBtnclicked: (){_ResetPassword();} ,
                horizontal: 80 , radiusButton: 40 , colorButton: anwhite , colorText: Colors.grey, heightButton: 60,
                textButton: " تم" , fontSize: 17
            ),

          ],
        ),



      ),
    );
  }
}