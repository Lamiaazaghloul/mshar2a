import 'package:demo/Tools/StyleApp.dart';
import 'package:demo/Tools/WidgetApp.dart';
import 'package:demo/pages/login/login.dart';
import 'package:flutter/material.dart';


class Page_SendCodeActive extends StatefulWidget {
  _BirdState createState() => new _BirdState();
}

class _BirdState extends State<Page_SendCodeActive> {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: "Cairo"),
      home:  new Scaffold(
        backgroundColor: anCyan,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            my_IconInContainer(color1: anwhite , heightWidth: 140 , widget: Icon(Icons.check , size: 100 ,color: Colors.green,)),

            Padding(
              padding: const EdgeInsets.all(20),
              child: Align(alignment: Alignment.center,
                  child: new Text("تم إرسال رمز إستعادة كلمة المرور إلي بريدك من فضلك قم بنالنقر علي الرابط لكي تمكن من إستعادة كلمة المرور" ,style: TextStyle(color: anwhite , fontSize: 18),textAlign: TextAlign.center,)
              ),
            ),


            SizedBox(height: 20),
            my_Button(
                onBtnclicked: (){
                   Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Login()),
  );
                } ,
                horizontal: 100 , radiusButton: 40 , colorButton: anwhite , colorText: Colors.grey, heightButton: 60,
                textButton: " تسجيل الدخول" , fontSize: 17
            ),



          ],
        ),),



      ),
    );
  }
}