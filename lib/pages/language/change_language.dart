import 'package:demo/Components/NavigationBar.dart';
import 'package:demo/Tools/print.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../allTranslations.dart';

class LangugeChange extends StatefulWidget {
    @override
    _LangugeChangeState createState() => _LangugeChangeState();
}

class _LangugeChangeState extends State<LangugeChange> {


    @override
    void initState(){
        super.initState();

        // Initializes a callback should something need 
        // to be done when the language is changed
        allTranslations.onLocaleChangedCallback = _onLocaleChanged;
    //    allTranslations.getPreferredLanguage();
    }

    ///
    /// If there is anything special to do when the user changes the language
    ///
    _onLocaleChanged() async {
        // do anything you need to do if the language changes
        printBlue('Language has been changed to: ${allTranslations.currentLanguage}');
        allTranslations.setPreferredLanguage(allTranslations.currentLanguage);
    }

    ///
    /// Main initialization
    ///
    @override
    Widget build(BuildContext context){
        return MaterialApp(
            localizationsDelegates: [
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
            ],
            // Tells the system which are the supported languages
            supportedLocales: allTranslations.supportedLocales(),

            home: HomePage(),
        );
    }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    // final String language = allTranslations.currentLanguage;
    // final String buttonText = language == 'fr' ? '=> English' : '=> Français';

  return Scaffold(
      appBar: AppBar(title: Text(allTranslations.text('main_title'))),
      body: Container(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            RaisedButton(
              child: Text('franch'),
              onPressed: () async {
                await allTranslations.setNewLanguage('fr');
                setState((){});
              },
            ),
             RaisedButton(
              child: Text('arabic'),
              onPressed: () async {
                await allTranslations.setNewLanguage('ar');
                setState((){});
              },
            ),
              RaisedButton(
              child: Text('english'),
              onPressed: () async {
                await allTranslations.setNewLanguage('en');
                setState((){});
              },
            ),

            RaisedButton(
              child: Text('english'),
              onPressed: ()  {
                Navigator.push(context, MaterialPageRoute(builder: (context) => NavigationBar()));
              },
            ),
            Text(allTranslations.text('main_body')),
          ],
        ),
      ),
    );
  }
}
